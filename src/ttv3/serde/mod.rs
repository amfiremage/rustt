use crate::ttv3::error::Error;
use crate::ttv3::untyped::Value;
use crate::ttv3::write_key_value;

use serde::ser::Serialize;
use std::fmt;

pub mod de;
pub mod de_read;
pub mod ser;

pub auto trait IsNotU8 {}
impl !IsNotU8 for u8 {}
impl IsNotU8 for Value {}

pub auto trait IsNotVU8 {}
impl !IsNotVU8 for Vec<u8> {}

pub auto trait IsNotCustom {}
impl !IsNotCustom for CustomNode {}

pub auto trait NotImplementKey {}
impl !NotImplementKey for Empty {}
impl !NotImplementKey for String {}
impl !NotImplementKey for &str {}
impl !NotImplementKey for Vec<u8> {}
impl !NotImplementKey for i8 {}
impl !NotImplementKey for i16 {}
impl !NotImplementKey for i32 {}
impl !NotImplementKey for i64 {}
impl !NotImplementKey for u8 {}
impl !NotImplementKey for u16 {}
impl !NotImplementKey for u32 {}
impl !NotImplementKey for u64 {}
impl !NotImplementKey for bool {}
impl !NotImplementKey for TtF32 {}
impl !NotImplementKey for TtF64 {}
impl !NotImplementKey for f32 {}
impl !NotImplementKey for f64 {}
impl !NotImplementKey for CustomNode {}

pub struct Empty {}

pub struct CustomNode {
	pub data: Vec<u8>,
	pub ttype: Type,
	pub children_length: usize,
}

pub struct CustomStructured {
	pub ttype: Type,
	pub children_length: usize,
}

#[derive(Hash, PartialEq, Eq, Debug)]
pub struct TtF32(u32);

impl TtF32 {
	pub fn from_f32(f: f32) -> TtF32 {
		TtF32 { 0: f.to_bits() }
	}
}

impl std::convert::Into<f32> for &TtF32 {
	#[inline]
	fn into(self) -> f32 {
		f32::from_bits(self.0)
	}
}

#[derive(Hash, PartialEq, Eq, Debug)]
pub struct TtF64(u64);

impl TtF64 {
	pub fn from_f64(f: f64) -> TtF64 {
		TtF64 { 0: f.to_bits() }
	}
}

impl std::convert::Into<f64> for &TtF64 {
	#[inline]
	fn into(self) -> f64 {
		f64::from_bits(self.0)
	}
}

pub trait KeyEncodable {
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error>;
	fn ttype(&self) -> u8;
	fn len(&self) -> usize;
	fn write_len(&self, buf: &mut [u8; 10]) -> usize;
}

pub trait ValueEncodable {
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error>;
	fn ttype(&self) -> u8;
	fn len(&self) -> usize;
	fn write_len(&self, buf: &mut [u8; 10]) -> usize;
	fn len_children(&self) -> usize;
	fn write_children<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error>;
}

#[derive(Clone, PartialEq, Eq, Copy)]
pub struct Type(u8);

pub const STRING: Type = Type::from_u8(1);
pub const BYTES: Type = Type::from_u8(2);
pub const I8: Type = Type::from_u8(3);
pub const I16: Type = Type::from_u8(4);
pub const I32: Type = Type::from_u8(5);
pub const I64: Type = Type::from_u8(6);
pub const U8: Type = Type::from_u8(7);
pub const U16: Type = Type::from_u8(8);
pub const U32: Type = Type::from_u8(9);
pub const U64: Type = Type::from_u8(10);
pub const BOOL: Type = Type::from_u8(11);
pub const F32: Type = Type::from_u8(12);
pub const F64: Type = Type::from_u8(13);
pub const MAP: Type = Type::from_u8(18);
pub const ARR: Type = Type::from_u8(19);

impl Default for Type {
	fn default() -> Type {
		Type::from_u8(0)
	}
}

impl fmt::Debug for Type {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "{}", self.as_str())
	}
}
impl Type {
	#[inline]
	pub const fn from_u8(v: u8) -> Type {
		Type { 0: v }
	}
	#[inline]
	pub const fn as_u8(self) -> u8 {
		self.0
	}
	pub fn as_str(self) -> &'static str {
		match self {
			STRING => "String",
			BYTES => "Bytes",
			I8 => "I8",
			I16 => "I16",
			I32 => "I32",
			I64 => "I64",
			U8 => "U8",
			U16 => "U16",
			U32 => "U32",
			U64 => "U64",
			BOOL => "Bool",
			F32 => "F32",
			F64 => "F64",
			MAP => "MAP",
			ARR => "ARR",
			_ => "Invalid",
		}
	}
}

impl KeyEncodable for Empty {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, _out: &mut W) -> Result<(), Error> {
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		0
	}
	#[inline]
	fn len(&self) -> usize {
		0
	}

	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		buf[0] = const_small_varint(0);
		1
	}
}

impl KeyEncodable for String {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		out.write_all(self.as_bytes())?;
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		STRING.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		self.as_bytes().len()
	}

	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		encode_var(self.len(), buf)
	}
}

impl KeyEncodable for &str {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		out.write_all(self.as_bytes())?;
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		STRING.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		self.as_bytes().len()
	}
	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		encode_var(self.as_bytes().len(), buf)
	}
}
impl KeyEncodable for &[u8] {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		out.write_all(self)?;
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		BYTES.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		<[u8]>::len(self)
	}
	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		encode_var(<[u8]>::len(self), buf)
	}
}

impl KeyEncodable for Vec<u8> {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		out.write_all(self)?;
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		BYTES.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		self.len()
	}
	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		encode_var(self.len(), buf)
	}
}

impl KeyEncodable for i8 {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		out.write_all(&encode_i8(*self))?;
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		I8.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		1
	}
	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		buf[0] = const_small_varint(1);
		1
	}
}

impl KeyEncodable for i16 {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		out.write_all(&encode_i16(*self))?;
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		I16.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		2
	}
	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		buf[0] = const_small_varint(2);
		1
	}
}

impl KeyEncodable for i32 {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		out.write_all(&encode_i32(*self))?;
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		I32.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		4
	}
	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		buf[0] = const_small_varint(4);
		1
	}
}

impl KeyEncodable for i64 {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		out.write_all(&encode_i64(*self))?;
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		I64.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		8
	}
	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		buf[0] = const_small_varint(8);
		1
	}
}

impl KeyEncodable for u8 {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		out.write_all(&encode_u8(*self))?;
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		U8.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		1
	}
	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		buf[0] = const_small_varint(1);
		1
	}
}

impl KeyEncodable for u16 {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		out.write_all(&encode_u16(*self))?;
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		U16.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		2
	}
	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		buf[0] = const_small_varint(2);
		1
	}
}

impl KeyEncodable for u32 {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		out.write_all(&encode_u32(*self))?;
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		U32.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		4
	}
	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		buf[0] = const_small_varint(4);
		1
	}
}

impl KeyEncodable for u64 {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		out.write_all(&encode_u64(*self))?;
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		U64.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		8
	}
	#[inline(always)]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		buf[0] = const_small_varint(8);
		1
	}
}
impl KeyEncodable for bool {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		out.write_all(match *self {
			true => &[1],
			false => &[0],
		})?;
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		BOOL.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		1
	}
	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		buf[0] = const_small_varint(1);
		1
	}
}

impl KeyEncodable for TtF32 {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		out.write_all(&encode_u32(self.0))?;
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		F32.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		4
	}
	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		buf[0] = const_small_varint(4);
		1
	}
}

impl KeyEncodable for TtF64 {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		out.write_all(&encode_u64(self.0))?;
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		F64.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		8
	}
	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		buf[0] = const_small_varint(8);
		1
	}
}

impl KeyEncodable for f32 {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		out.write_all(&encode_f32(*self))?;
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		F32.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		4
	}
	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		buf[0] = const_small_varint(4);
		1
	}
}

impl KeyEncodable for f64 {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		out.write_all(&encode_f64(*self))?;
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		F64.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		8
	}
	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		buf[0] = const_small_varint(8);
		1
	}
}

impl<'a> KeyEncodable for &'a CustomNode {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		out.write_all(&self.data)?;
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		self.ttype.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		self.data.len()
	}
	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		encode_var(self.data.len(), buf)
	}
}

impl<T: Serialize> KeyEncodable for &T {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		self.serialize(ser::KeyWriter { writer: out })
	}
	#[inline]
	fn ttype(&self) -> u8 {
		match self.serialize(ser::KeyTyper {}) {
			Ok(t) => t,
			Err(err) => panic!(err),
		}
	}
	#[inline]
	fn len(&self) -> usize {
		match self.serialize(ser::KeySizer {}) {
			Ok(l) => l,
			Err(err) => panic!(err),
		}
	}

	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		encode_var(
			match self.serialize(ser::KeySizer {}) {
				Ok(l) => l,
				Err(err) => panic!(err),
			},
			buf,
		)
	}
}

impl<K: KeyEncodable + IsNotVU8 + IsNotCustom> ValueEncodable for K {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		KeyEncodable::write_bytes(self, out)
	}
	#[inline]
	fn ttype(&self) -> u8 {
		KeyEncodable::ttype(self)
	}
	#[inline]
	fn len(&self) -> usize {
		KeyEncodable::len(self)
	}
	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		encode_var(KeyEncodable::len(self), buf)
	}

	#[inline]
	fn len_children(&self) -> usize {
		0
	}
	#[inline]
	fn write_children<W: std::io::Write>(&self, _out: &mut W) -> Result<(), Error> {
		Ok(())
	}
}

impl<K: KeyEncodable + std::hash::Hash + std::cmp::Eq, V: ValueEncodable> ValueEncodable
	for std::collections::HashMap<K, V>
{
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, _out: &mut W) -> Result<(), Error> {
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		MAP.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		0
	}
	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		buf[0] = const_small_varint(0);
		1
	}

	#[inline]
	fn len_children(&self) -> usize {
		self.len()
	}
	fn write_children<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		for (k, v) in self.iter() {
			write_key_value(k, v, out)?;
		}
		Ok(())
	}
}
impl ValueEncodable for String {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		out.write_all(self.as_bytes())?;
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		STRING.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		self.as_bytes().len()
	}
	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		encode_var(self.as_bytes().len(), buf)
	}

	#[inline]
	fn len_children(&self) -> usize {
		0
	}
	#[inline]
	fn write_children<W: std::io::Write>(&self, _out: &mut W) -> Result<(), Error> {
		Ok(())
	}
}

impl ValueEncodable for Vec<u8> {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		out.write_all(self)?;
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		BYTES.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		self.len()
	}
	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		encode_var(self.len(), buf)
	}

	#[inline]
	fn len_children(&self) -> usize {
		0
	}
	#[inline]
	fn write_children<W: std::io::Write>(&self, _out: &mut W) -> Result<(), Error> {
		Ok(())
	}
}

impl<V: ValueEncodable + IsNotU8> ValueEncodable for Vec<V> {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, _out: &mut W) -> Result<(), Error> {
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		ARR.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		0
	}
	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		buf[0] = const_small_varint(0);
		1
	}

	#[inline]
	fn len_children(&self) -> usize {
		self.len()
	}
	fn write_children<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		for v in self.iter() {
			write_key_value(&Empty {}, v, out)?;
		}
		Ok(())
	}
}

impl ValueEncodable for CustomNode {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, out: &mut W) -> Result<(), Error> {
		out.write_all(&self.data).unwrap();
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		self.ttype.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		self.data.len()
	}
	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		encode_var(self.data.len(), buf)
	}

	#[inline]
	fn len_children(&self) -> usize {
		self.children_length
	}
	#[inline]
	fn write_children<W: std::io::Write>(&self, _out: &mut W) -> Result<(), Error> {
		Ok(())
	}
}

impl ValueEncodable for CustomStructured {
	#[inline]
	fn write_bytes<W: std::io::Write>(&self, _out: &mut W) -> Result<(), Error> {
		Ok(())
	}
	#[inline]
	fn ttype(&self) -> u8 {
		self.ttype.as_u8()
	}
	#[inline]
	fn len(&self) -> usize {
		0
	}
	#[inline]
	fn write_len(&self, buf: &mut [u8; 10]) -> usize {
		buf[0] = const_small_varint(0);
		1
	}

	#[inline]
	fn len_children(&self) -> usize {
		self.children_length
	}
	#[inline]
	fn write_children<W: std::io::Write>(&self, _out: &mut W) -> Result<(), Error> {
		Ok(())
	}
}

#[inline]
pub fn encode_i8(u: i8) -> [u8; 1] {
	u.to_le_bytes()
}

#[inline]
pub fn encode_i16(u: i16) -> [u8; 2] {
	u.to_le_bytes()
}

#[inline]
pub fn encode_i32(u: i32) -> [u8; 4] {
	u.to_le_bytes()
}

#[inline]
pub fn encode_i64(u: i64) -> [u8; 8] {
	u.to_le_bytes()
}

#[inline]
pub fn encode_u8(u: u8) -> [u8; 1] {
	u.to_le_bytes()
}

#[inline]
pub fn encode_u16(u: u16) -> [u8; 2] {
	u.to_le_bytes()
}

#[inline]
pub fn encode_u32(u: u32) -> [u8; 4] {
	u.to_le_bytes()
}

#[inline]
pub fn encode_u64(u: u64) -> [u8; 8] {
	u.to_le_bytes()
}

#[inline]
pub fn encode_f32(u: f32) -> [u8; 4] {
	u.to_le_bytes()
}

#[inline]
pub fn encode_f64(u: f64) -> [u8; 8] {
	u.to_le_bytes()
}

#[inline]
fn const_small_varint(size: u8) -> u8 {
	if size < 128 {
		return size;
	}
	panic!("const_small_varint can only be called for ints smaller than 128")
}

#[inline(always)]
pub(crate) fn encode_var(mut u: usize, dst: &mut [u8; 10]) -> usize {
	let mut i = 0;
	while u >= 0x80 {
		dst[i] = u as u8 | 0x80;
		u >>= 7;
		i += 1;
	}
	dst[i] = u as u8;
	i + 1
}
