use crate::ttv3::decode_var_reader;
use crate::ttv3::discard_var_reader;
use crate::ttv3::error::Error;
use crate::ttv3::read_byte;
use crate::ttv3::serde;
use crate::ttv3::serde::de::Deserializer as Deserializer_vec;
use crate::ttv3::serde::Type;
use crate::ttv3::ReadNode;
use ::serde::de::{
	self, DeserializeSeed, Deserializer as ser_Deserializer, EnumAccess, MapAccess, SeqAccess,
	VariantAccess, Visitor,
};
use std::io::Read;

//TODO: make sure to check all bounds for fixed-size values like ints to not panic

pub struct Deserializer<'de, R: Read> {
	pub input: &'de mut R,
	value: ReadNode,
	buf: String,
}

impl<'de, R: Read> Deserializer<'de, R> {
	#[inline]
	fn read_node(&mut self) {
		let _ = self.value.read_lengths(self.input);
	}

	#[inline]
	fn read_value_type(&mut self) -> Result<Type, Error> {
		match read_byte(self.input) {
			Some(b) => Ok(Type::from_u8(b)),
			None => Err(Error::OutOfSpace()),
		}
	}

	#[inline]
	fn read_value_to_str(&mut self) {
		self.buf.clear();
		let _ = self
			.input
			.take(self.value.value_length as u64)
			.read_to_string(&mut self.buf);
	}

	#[inline]
	fn read_value_to_vec(&mut self, buf: &mut Vec<u8>) {
		let _ = self
			.input
			.take(self.value.value_length as u64)
			.read_to_end(buf);
	}

	#[inline]
	fn read_key_to_str(&mut self) {
		self.buf.clear();
		let _ = self
			.input
			.take(self.value.key_length as u64)
			.read_to_string(&mut self.buf);
	}

	#[inline]
	fn discard_value_key_type_key(&mut self) {
		let _ = self
			.input
			.take((self.value.value_length + 1 + self.value.key_length) as u64)
			.read_to_end(&mut Vec::new());
	}

	#[inline]
	fn discard_key_type_key(&mut self) {
		let _ = self
			.input
			.take((1 + self.value.key_length) as u64)
			.read_to_end(&mut Vec::new());
	}

	fn read_children_length(&mut self) -> Result<usize, Error> {
		decode_var_reader(self.input)
	}

	fn discard_children_length(&mut self) {
		discard_var_reader(self.input);
	}

	// By convention, `Deserializer` constructors are named like `from_xyz`.
	// That way basic use cases are satisfied by something like
	// `serde_json::from_str(...)` while advanced use cases that require a
	// deserializer can make one with `serde_json::Deserializer::from_str(...)`.
	pub fn from_reader(input: &'de mut R) -> Self {
		Deserializer {
			input: input,
			value: ReadNode::default(),
			buf: String::new(),
		}
	}
	#[inline]
	pub fn read_str<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		if typ != serde::STRING && typ != serde::BYTES {
			Err(Error::Expected((serde::STRING, typ)))
		} else {
			self.read_value_to_str();
			self.discard_key_type_key();
			self.discard_children_length();

			visitor.visit_str(self.buf.as_str())
		}
	}
	pub fn read_bytes<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		if typ != serde::STRING && typ != serde::BYTES {
			Err(Error::Expected((serde::BYTES, typ)))
		} else {
			self.read_value_to_str();
			self.discard_key_type_key();
			self.discard_children_length();
			visitor.visit_bytes(self.buf.as_bytes())
		}
	}

	pub fn read_i8<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		check_type(typ, serde::I16)?;
		let mut buf: [u8; 1] = Default::default();
		self.input.read_exact(&mut buf)?;
		self.discard_key_type_key();
		self.discard_children_length();

		visitor.visit_i8(i8::from_le_bytes(buf))
	}

	pub fn read_i16<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		check_type(typ, serde::I8)?;
		let mut buf: [u8; 2] = Default::default();
		self.input.read_exact(&mut buf)?;
		self.discard_key_type_key();
		self.discard_children_length();

		visitor.visit_i16(i16::from_le_bytes(buf))
	}
	pub fn read_i32<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		check_type(typ, serde::I32)?;
		let mut buf: [u8; 4] = Default::default();
		self.input.read_exact(&mut buf)?;
		self.discard_key_type_key();
		self.discard_children_length();

		visitor.visit_i32(i32::from_le_bytes(buf))
	}
	pub fn read_i64<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		check_type(typ, serde::I64)?;
		let mut buf: [u8; 8] = Default::default();
		self.input.read_exact(&mut buf)?;
		self.discard_key_type_key();
		self.discard_children_length();

		visitor.visit_i64(i64::from_le_bytes(buf))
	}
	pub fn read_u8<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		check_type(typ, serde::U16)?;
		let mut buf: [u8; 1] = Default::default();
		self.input.read_exact(&mut buf)?;
		self.discard_key_type_key();
		self.discard_children_length();

		visitor.visit_u8(u8::from_le_bytes(buf))
	}

	pub fn read_u16<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		check_type(typ, serde::I8)?;
		let mut buf: [u8; 2] = Default::default();
		self.input.read_exact(&mut buf)?;
		self.discard_key_type_key();
		self.discard_children_length();

		visitor.visit_u16(u16::from_le_bytes(buf))
	}
	pub fn read_u32<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		check_type(typ, serde::U32)?;
		let mut buf: [u8; 4] = Default::default();
		self.input.read_exact(&mut buf)?;
		self.discard_key_type_key();
		self.discard_children_length();

		visitor.visit_u32(u32::from_le_bytes(buf))
	}
	pub fn read_u64<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		check_type(typ, serde::U64)?;
		let mut buf: [u8; 8] = Default::default();
		self.input.read_exact(&mut buf)?;
		self.discard_key_type_key();
		self.discard_children_length();

		visitor.visit_u64(u64::from_le_bytes(buf))
	}

	pub fn read_bool<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		check_type(typ, serde::BOOL)?;
		let b = match read_byte(self.input) {
			Some(b) => b,
			None => return Err(Error::OutOfSpace()),
		};
		self.discard_key_type_key();
		self.discard_children_length();

		visitor.visit_bool(b == 1)
	}

	pub fn read_f32<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		check_type(typ, serde::F32)?;
		let mut buf: [u8; 4] = Default::default();
		self.input.read_exact(&mut buf)?;
		self.discard_key_type_key();
		self.discard_children_length();

		visitor.visit_f32(f32::from_le_bytes(buf))
	}
	pub fn read_f64<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		check_type(typ, serde::F64)?;
		let mut buf: [u8; 8] = Default::default();
		self.input.read_exact(&mut buf)?;
		self.discard_key_type_key();
		self.discard_children_length();

		visitor.visit_f64(f64::from_le_bytes(buf))
	}

	pub fn decode_i8(bytes: &[u8], typ: Type) -> Result<i8, Error> {
		check_type(typ, serde::I8)?;

		let mut buf: [u8; 1] = Default::default();
		buf.clone_from_slice(bytes);

		Ok(i8::from_le_bytes(buf))
	}
	pub fn decode_i16(bytes: &[u8], typ: Type) -> Result<i16, Error> {
		check_type(typ, serde::I16)?;

		let mut buf: [u8; 2] = Default::default();
		buf.clone_from_slice(bytes);

		Ok(i16::from_le_bytes(buf))
	}
	pub fn decode_i32(bytes: &[u8], typ: Type) -> Result<i32, Error> {
		check_type(typ, serde::I32)?;

		let mut buf: [u8; 4] = Default::default();
		buf.clone_from_slice(bytes);

		Ok(i32::from_le_bytes(buf))
	}
	pub fn decode_i64(bytes: &[u8], typ: Type) -> Result<i64, Error> {
		check_type(typ, serde::I64)?;

		let mut buf: [u8; 8] = Default::default();
		buf.clone_from_slice(bytes);

		Ok(i64::from_le_bytes(buf))
	}
	pub fn decode_u8(bytes: &[u8], typ: Type) -> Result<u8, Error> {
		check_type(typ, serde::U8)?;

		let mut buf: [u8; 1] = Default::default();
		buf.clone_from_slice(bytes);

		Ok(u8::from_le_bytes(buf))
	}
	pub fn decode_u16(bytes: &[u8], typ: Type) -> Result<u16, Error> {
		check_type(typ, serde::U16)?;

		let mut buf: [u8; 2] = Default::default();
		buf.clone_from_slice(bytes);

		Ok(u16::from_le_bytes(buf))
	}
	pub fn decode_u32(bytes: &[u8], typ: Type) -> Result<u32, Error> {
		check_type(typ, serde::U32)?;

		let mut buf: [u8; 4] = Default::default();
		buf.clone_from_slice(bytes);

		Ok(u32::from_le_bytes(buf))
	}
	pub fn decode_u64(bytes: &[u8], typ: Type) -> Result<u64, Error> {
		check_type(typ, serde::U64)?;

		let mut buf: [u8; 8] = Default::default();
		buf.clone_from_slice(bytes);

		Ok(u64::from_le_bytes(buf))
	}

	pub fn decode_bool(bytes: &[u8], typ: Type) -> Result<bool, Error> {
		check_type(typ, serde::BOOL)?;
		Ok(bytes[0] == 1)
	}

	pub fn decode_f32(bytes: &[u8], typ: Type) -> Result<f32, Error> {
		check_type(typ, serde::F32)?;

		let mut buf: [u8; 4] = Default::default();
		buf.clone_from_slice(bytes);

		Ok(f32::from_bits(u32::from_le_bytes(buf)))
	}
	pub fn decode_f64(bytes: &[u8], typ: Type) -> Result<f64, Error> {
		check_type(typ, serde::F64)?;

		let mut buf: [u8; 8] = Default::default();
		buf.clone_from_slice(bytes);

		Ok(f64::from_bits(u64::from_le_bytes(buf)))
	}
}

impl<'de, 'a, R: Read> ser_Deserializer<'de> for &'a mut Deserializer<'de, R> {
	type Error = Error;
	fn deserialize_any<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.read_node();

		let typ = self.read_value_type()?;
		match typ {
			serde::STRING => self.read_str(typ, visitor),
			serde::BYTES => self.read_bytes(typ, visitor),
			serde::I8 => self.read_i8(typ, visitor),
			serde::I16 => self.read_i16(typ, visitor),
			serde::I32 => self.read_i32(typ, visitor),
			serde::I64 => self.read_i64(typ, visitor),
			serde::U8 => self.read_u8(typ, visitor),
			serde::U16 => self.read_u16(typ, visitor),
			serde::U32 => self.read_u32(typ, visitor),
			serde::U64 => self.read_u64(typ, visitor),
			serde::BOOL => self.read_bool(typ, visitor),
			serde::F32 => self.read_f32(typ, visitor),
			serde::F64 => self.read_f64(typ, visitor),
			serde::MAP => {
				self.discard_value_key_type_key();
				// Give the visitor access to each entry of the map.
				let len = self.read_children_length()?;
				visitor.visit_map(MapDecoder::new(
					self,
					len,
					&mut ValueNode {
						value_data: Vec::new(),
						value_type: Type::from_u8(0),
					},
				))
			}
			serde::ARR => {
				self.discard_value_key_type_key();

				let len = self.read_children_length()?;
				visitor.visit_seq(ListDecoder::new(self, len))
			}
			_ => Err(Error::new("Cannot decode Invalid type")),
		}
	}
	fn deserialize_str<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.read_node();
		let typ = self.read_value_type()?;
		self.read_str(typ, visitor)
	}

	#[inline]
	fn deserialize_char<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_str(visitor)
	}

	#[inline]
	fn deserialize_string<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_str(visitor)
	}

	fn deserialize_bytes<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.read_node();
		let typ = self.read_value_type()?;
		self.read_bytes(typ, visitor)
	}
	#[inline]
	fn deserialize_byte_buf<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_bytes(visitor)
	}

	/// There are no "null" values in TT, so None is not an option.
	#[inline]
	fn deserialize_option<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		visitor.visit_some(self)
	}

	fn deserialize_i8<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.read_node();
		let typ = self.read_value_type()?;
		self.read_i8(typ, visitor)
	}

	fn deserialize_i16<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.read_node();
		let typ = self.read_value_type()?;
		self.read_i16(typ, visitor)
	}

	fn deserialize_i32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.read_node();
		let typ = self.read_value_type()?;
		self.read_i32(typ, visitor)
	}

	fn deserialize_i64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.read_node();
		let typ = self.read_value_type()?;
		self.read_i64(typ, visitor)
	}

	fn deserialize_u8<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.read_node();
		let typ = self.read_value_type()?;
		self.read_u8(typ, visitor)
	}

	fn deserialize_u16<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.read_node();
		let typ = self.read_value_type()?;
		self.read_u16(typ, visitor)
	}

	fn deserialize_u32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.read_node();
		let typ = self.read_value_type()?;
		self.read_u32(typ, visitor)
	}

	fn deserialize_u64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.read_node();
		let typ = self.read_value_type()?;
		self.read_u64(typ, visitor)
	}

	fn deserialize_bool<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.read_node();
		let typ = self.read_value_type()?;
		self.read_bool(typ, visitor)
	}

	fn deserialize_f32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.read_node();
		let typ = self.read_value_type()?;
		self.read_f32(typ, visitor)
	}

	fn deserialize_f64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.read_node();
		let typ = self.read_value_type()?;
		self.read_f64(typ, visitor)
	}

	fn deserialize_seq<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.read_node();

		let typ = self.read_value_type()?;
		check_type(typ, serde::ARR)?;

		self.discard_children_length();

		let childr_len = self.read_children_length()?;
		visitor.visit_seq(ListDecoder::new(self, childr_len))
	}

	// Tuples look just like sequences in TT, but the length needs to be checked
	fn deserialize_tuple<V>(self, len: usize, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.read_node();
		let typ = self.read_value_type()?;
		check_type(typ, serde::ARR)?;

		self.discard_children_length();

		let childr_len = self.read_children_length()?;
		if childr_len == len {
			visitor.visit_seq(ListDecoder::new(self, len))
		} else {
			Err(Error::Message(format!(
				"The length doesnt match: expected {}, got {}",
				len, childr_len
			)))
		}
	}

	// Tuple structs look just like sequences in JSON.
	#[inline]
	fn deserialize_tuple_struct<V>(
		self,
		_name: &'static str,
		len: usize,
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_tuple(len, visitor)
	}

	// Much like `deserialize_seq` but calls the visitors `visit_map` method
	// with a `MapAccess` implementation, rather than the visitor's `visit_seq`
	// method with a `SeqAccess` implementation.
	fn deserialize_map<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.read_node();

		let typ = self.read_value_type()?;
		check_type(typ, serde::MAP)?;
		self.discard_value_key_type_key();
		// Give the visitor access to each entry of the map.
		let len = self.read_children_length()?;
		visitor.visit_map(MapDecoder::new(
			self,
			len,
			&mut ValueNode {
				value_data: Vec::new(),
				value_type: Type::from_u8(0),
			},
		))
	}
	// Structs look just like maps in TT.
	#[inline]
	fn deserialize_struct<V>(
		self,
		_name: &'static str,
		_fields: &'static [&'static str],
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_map(visitor)
	}
	fn deserialize_enum<V>(
		self,
		_name: &'static str,
		_variants: &'static [&'static str],
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.read_node();
		let typ = self.read_value_type()?;
		check_type(typ, serde::MAP)?;
		self.discard_value_key_type_key();
		self.discard_children_length();
		visitor.visit_enum(Enum::new(self))
	}
	/// We never encode Unit values, so we shouldnt decode them either
	#[inline]
	fn deserialize_unit<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		visitor.visit_unit()
	}

	#[inline]
	fn deserialize_unit_struct<V>(
		self,
		_name: &'static str,
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_unit(visitor)
	}

	// Serializers are encouraged to treat newtype structs as
	// insignificant wrappers around the data they contain. That means not
	// parsing anything other than the contained value.
	#[inline]
	fn deserialize_newtype_struct<V>(
		self,
		_name: &'static str,
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		visitor.visit_newtype_struct(self)
	}

	#[inline]
	fn deserialize_identifier<V>(self, _visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		Err(Error::new(
			"TT does not support Deserializer::deserialize_identifier",
		))
	}
	#[inline]
	fn deserialize_ignored_any<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_any(visitor)
	}
}
//KeyDesirializer is a one-time decoder for a single key
pub struct KeyDeserializer<'a, 'de: 'a, R: Read> {
	de: &'a mut Deserializer<'de, R>,
	v: &'a mut ValueNode,
}

impl<'de, 'a, R: Read> KeyDeserializer<'a, 'de, R> {
	pub fn read_str<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		if typ != serde::STRING && typ != serde::BYTES {
			Err(Error::Expected((serde::STRING, typ)))
		} else {
			self.de.read_key_to_str();
			visitor.visit_str(self.de.buf.as_str())
		}
	}
	pub fn read_bytes<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		if typ != serde::STRING && typ != serde::BYTES {
			Err(Error::Expected((serde::BYTES, typ)))
		} else {
			self.de.read_key_to_str();
			visitor.visit_bytes(self.de.buf.as_bytes())
		}
	}

	pub fn read_i8<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		check_type(typ, serde::I16)?;
		let mut buf: [u8; 1] = Default::default();
		self.de.input.read_exact(&mut buf)?;

		visitor.visit_i8(i8::from_le_bytes(buf))
	}

	pub fn read_i16<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		check_type(typ, serde::I8)?;
		let mut buf: [u8; 2] = Default::default();
		self.de.input.read_exact(&mut buf)?;

		visitor.visit_i16(i16::from_le_bytes(buf))
	}
	pub fn read_i32<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		check_type(typ, serde::I32)?;
		let mut buf: [u8; 4] = Default::default();
		self.de.input.read_exact(&mut buf)?;

		visitor.visit_i32(i32::from_le_bytes(buf))
	}
	pub fn read_i64<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		check_type(typ, serde::I64)?;
		let mut buf: [u8; 8] = Default::default();
		self.de.input.read_exact(&mut buf)?;

		visitor.visit_i64(i64::from_le_bytes(buf))
	}
	pub fn read_u8<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		check_type(typ, serde::U16)?;
		let mut buf: [u8; 1] = Default::default();
		self.de.input.read_exact(&mut buf)?;

		visitor.visit_u8(u8::from_le_bytes(buf))
	}

	pub fn read_u16<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		check_type(typ, serde::I8)?;
		let mut buf: [u8; 2] = Default::default();
		self.de.input.read_exact(&mut buf)?;

		visitor.visit_u16(u16::from_le_bytes(buf))
	}
	pub fn read_u32<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		check_type(typ, serde::U32)?;
		let mut buf: [u8; 4] = Default::default();
		self.de.input.read_exact(&mut buf)?;

		visitor.visit_u32(u32::from_le_bytes(buf))
	}
	pub fn read_u64<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		check_type(typ, serde::U64)?;
		let mut buf: [u8; 8] = Default::default();
		self.de.input.read_exact(&mut buf)?;

		visitor.visit_u64(u64::from_le_bytes(buf))
	}

	pub fn read_bool<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		check_type(typ, serde::BOOL)?;
		let b = match read_byte(self.de.input) {
			Some(b) => b,
			None => return Err(Error::OutOfSpace()),
		};

		visitor.visit_bool(b == 1)
	}

	pub fn read_f32<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		check_type(typ, serde::F32)?;
		let mut buf: [u8; 4] = Default::default();
		self.de.input.read_exact(&mut buf)?;

		visitor.visit_f32(f32::from_le_bytes(buf))
	}
	pub fn read_f64<V>(&mut self, typ: Type, visitor: V) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		check_type(typ, serde::F64)?;
		let mut buf: [u8; 8] = Default::default();
		self.de.input.read_exact(&mut buf)?;

		visitor.visit_f64(f64::from_le_bytes(buf))
	}
}

impl<'de, 'a, R: Read> ser_Deserializer<'de> for &'a mut KeyDeserializer<'a, 'de, R> {
	type Error = Error;
	fn deserialize_any<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.read_node();
		self.v.value_type = self.de.read_value_type()?;
		self.de.read_value_to_vec(&mut self.v.value_data);

		let typ = self.de.read_value_type()?;
		match typ {
			serde::STRING => self.read_str(typ, visitor),
			serde::BYTES => self.read_bytes(typ, visitor),
			serde::I8 => self.read_i8(typ, visitor),
			serde::I16 => self.read_i16(typ, visitor),
			serde::I32 => self.read_i32(typ, visitor),
			serde::I64 => self.read_i64(typ, visitor),
			serde::U8 => self.read_u8(typ, visitor),
			serde::U16 => self.read_u16(typ, visitor),
			serde::U32 => self.read_u32(typ, visitor),
			serde::U64 => self.read_u64(typ, visitor),
			serde::BOOL => self.read_bool(typ, visitor),
			serde::F32 => self.read_f32(typ, visitor),
			serde::F64 => self.read_f64(typ, visitor),
			serde::MAP => Err(Error::ImpossibleKey(serde::MAP)),
			serde::ARR => Err(Error::ImpossibleKey(serde::ARR)),
			_ => Err(Error::new("Cannot decode Invalid type")),
		}
	}

	fn deserialize_str<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.read_node();
		self.v.value_type = self.de.read_value_type()?;
		self.de.read_value_to_vec(&mut self.v.value_data);
		let typ = self.de.read_value_type()?;
		self.read_str(typ, visitor)
	}

	#[inline]
	fn deserialize_char<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_str(visitor)
	}

	#[inline]
	fn deserialize_string<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_str(visitor)
	}

	fn deserialize_bytes<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.read_node();
		self.v.value_type = self.de.read_value_type()?;
		self.de.read_value_to_vec(&mut self.v.value_data);
		let typ = self.de.read_value_type()?;
		self.read_str(typ, visitor)
	}
	#[inline]
	fn deserialize_byte_buf<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_bytes(visitor)
	}

	/// There are no "null" values in TT, so None is not an option.
	#[inline]
	fn deserialize_option<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		visitor.visit_some(self)
	}

	fn deserialize_i8<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.read_node();
		self.v.value_type = self.de.read_value_type()?;
		self.de.read_value_to_vec(&mut self.v.value_data);
		let typ = self.de.read_value_type()?;
		self.read_str(typ, visitor)
	}

	fn deserialize_i16<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.read_node();
		self.v.value_type = self.de.read_value_type()?;
		self.de.read_value_to_vec(&mut self.v.value_data);
		let typ = self.de.read_value_type()?;
		self.read_str(typ, visitor)
	}

	fn deserialize_i32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.read_node();
		self.v.value_type = self.de.read_value_type()?;
		self.de.read_value_to_vec(&mut self.v.value_data);
		let typ = self.de.read_value_type()?;
		self.read_str(typ, visitor)
	}

	fn deserialize_i64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.read_node();
		self.v.value_type = self.de.read_value_type()?;
		self.de.read_value_to_vec(&mut self.v.value_data);
		let typ = self.de.read_value_type()?;
		self.read_str(typ, visitor)
	}

	fn deserialize_u8<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.read_node();
		self.v.value_type = self.de.read_value_type()?;
		self.de.read_value_to_vec(&mut self.v.value_data);
		let typ = self.de.read_value_type()?;
		self.read_str(typ, visitor)
	}

	fn deserialize_u16<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.read_node();
		self.v.value_type = self.de.read_value_type()?;
		self.de.read_value_to_vec(&mut self.v.value_data);
		let typ = self.de.read_value_type()?;
		self.read_str(typ, visitor)
	}

	fn deserialize_u32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.read_node();
		self.v.value_type = self.de.read_value_type()?;
		self.de.read_value_to_vec(&mut self.v.value_data);
		let typ = self.de.read_value_type()?;
		self.read_str(typ, visitor)
	}

	fn deserialize_u64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.read_node();
		self.v.value_type = self.de.read_value_type()?;
		self.de.read_value_to_vec(&mut self.v.value_data);
		let typ = self.de.read_value_type()?;
		self.read_str(typ, visitor)
	}

	fn deserialize_bool<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.read_node();
		self.v.value_type = self.de.read_value_type()?;
		self.de.read_value_to_vec(&mut self.v.value_data);
		let typ = self.de.read_value_type()?;
		self.read_str(typ, visitor)
	}
	fn deserialize_f32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.read_node();
		self.v.value_type = self.de.read_value_type()?;
		self.de.read_value_to_vec(&mut self.v.value_data);
		let typ = self.de.read_value_type()?;
		self.read_str(typ, visitor)
	}

	fn deserialize_f64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.read_node();
		self.v.value_type = self.de.read_value_type()?;
		self.de.read_value_to_vec(&mut self.v.value_data);
		let typ = self.de.read_value_type()?;
		self.read_str(typ, visitor)
	}

	#[inline]
	fn deserialize_seq<V>(self, _visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		Err(Error::ImpossibleKey(serde::ARR))
	}

	// Tuples look just like sequences in TT, but the length needs to be checked
	#[inline]
	fn deserialize_tuple<V>(self, _len: usize, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_seq(visitor)
	}
	// Tuple structs look just like sequences in JSON.
	#[inline]
	fn deserialize_tuple_struct<V>(
		self,
		_name: &'static str,
		_len: usize,
		visitor: V,
	) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_seq(visitor)
	}
	/// We never encode Unit values, so we shouldnt decode them either
	#[inline]
	fn deserialize_unit<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		visitor.visit_unit()
	}

	#[inline]
	fn deserialize_unit_struct<V>(
		self,
		_name: &'static str,
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_unit(visitor)
	}

	// Serializers are encouraged to treat newtype structs as
	// insignificant wrappers around the data they contain. That means not
	// parsing anything other than the contained value.
	#[inline]
	fn deserialize_newtype_struct<V>(
		self,
		_name: &'static str,
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		visitor.visit_newtype_struct(self)
	}
	// Much like `deserialize_seq` but calls the visitors `visit_map` method
	// with a `MapAccess` implementation, rather than the visitor's `visit_seq`
	// method with a `SeqAccess` implementation.
	fn deserialize_map<V>(self, _visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		Err(Error::ImpossibleKey(serde::MAP))
	}
	// Structs look just like maps in TT.
	#[inline]
	fn deserialize_struct<V>(
		self,
		_name: &'static str,
		_fields: &'static [&'static str],
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_map(visitor)
	}
	#[inline]
	fn deserialize_enum<V>(
		self,
		_name: &'static str,
		_variants: &'static [&'static str],
		_visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		Err(Error::new("Cannot decode Enum as key"))
	}

	#[inline]
	fn deserialize_identifier<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_any(visitor)
	}

	#[inline]
	fn deserialize_ignored_any<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_any(visitor)
	}
}

pub struct ValueNode {
	value_data: Vec<u8>,
	value_type: Type,
}

//MapValueDeserializer is a one-time decoder for a single value in a Map type
pub struct MapValueDeserializer<'a, 'de: 'a, R: Read> {
	de: &'a mut Deserializer<'de, R>,
	v: &'a ValueNode,
}

impl<'de, 'a, R: Read> ser_Deserializer<'de> for &'a mut MapValueDeserializer<'a, 'de, R> {
	type Error = Error;
	fn deserialize_any<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		match self.v.value_type {
			serde::STRING => self.deserialize_str(visitor),
			serde::BYTES => self.deserialize_bytes(visitor),
			serde::I8 => self.deserialize_i8(visitor),
			serde::I16 => self.deserialize_i16(visitor),
			serde::I32 => self.deserialize_i32(visitor),
			serde::I64 => self.deserialize_i64(visitor),
			serde::U8 => self.deserialize_u8(visitor),
			serde::U16 => self.deserialize_u16(visitor),
			serde::U32 => self.deserialize_u32(visitor),
			serde::U64 => self.deserialize_u64(visitor),
			serde::BOOL => self.deserialize_bool(visitor),
			serde::F32 => self.deserialize_f32(visitor),
			serde::F64 => self.deserialize_f64(visitor),
			serde::MAP => {
				let len = self.de.read_children_length()?;
				visitor.visit_map(MapDecoder::new(
					self.de,
					len,
					&mut ValueNode {
						value_data: Vec::new(),
						value_type: Type::from_u8(0),
					},
				))
			}
			serde::ARR => {
				let len = self.de.read_children_length()?;

				visitor.visit_seq(ListDecoder::new(self.de, len))
			}
			_ => Err(Error::new("Cannot decode Invalid type")),
		}
	}
	fn deserialize_str<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.discard_children_length();
		if self.v.value_type != serde::STRING && self.v.value_type != serde::BYTES {
			Err(Error::Expected((serde::STRING, self.v.value_type)))
		} else {
			visitor.visit_str(std::str::from_utf8(&self.v.value_data[..])?)
		}
	}

	#[inline]
	fn deserialize_char<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_str(visitor)
	}

	#[inline]
	fn deserialize_string<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_str(visitor)
	}

	fn deserialize_bytes<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.discard_children_length();
		if self.v.value_type != serde::STRING && self.v.value_type != serde::BYTES {
			Err(Error::Expected((serde::BYTES, self.v.value_type)))
		} else {
			visitor.visit_bytes(&self.v.value_data[..])
		}
	}
	#[inline]
	fn deserialize_byte_buf<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_bytes(visitor)
	}

	/// There are no "null" values in TT, so None is not an option.
	#[inline]
	fn deserialize_option<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		visitor.visit_some(self)
	}

	fn deserialize_i8<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.discard_children_length();
		visitor.visit_i8(Deserializer_vec::decode_i8(
			&self.v.value_data[..],
			self.v.value_type,
		)?)
	}

	fn deserialize_i16<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.discard_children_length();
		visitor.visit_i16(Deserializer_vec::decode_i16(
			&self.v.value_data[..],
			self.v.value_type,
		)?)
	}

	fn deserialize_i32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.discard_children_length();
		visitor.visit_i32(Deserializer_vec::decode_i32(
			&self.v.value_data[..],
			self.v.value_type,
		)?)
	}

	fn deserialize_i64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.discard_children_length();
		visitor.visit_i64(Deserializer_vec::decode_i64(
			&self.v.value_data[..],
			self.v.value_type,
		)?)
	}

	fn deserialize_u8<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.discard_children_length();
		visitor.visit_u8(Deserializer_vec::decode_u8(
			&self.v.value_data[..],
			self.v.value_type,
		)?)
	}

	fn deserialize_u16<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.discard_children_length();
		visitor.visit_u16(Deserializer_vec::decode_u16(
			&self.v.value_data[..],
			self.v.value_type,
		)?)
	}

	fn deserialize_u32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.discard_children_length();
		visitor.visit_u32(Deserializer_vec::decode_u32(
			&self.v.value_data[..],
			self.v.value_type,
		)?)
	}

	fn deserialize_u64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.discard_children_length();
		visitor.visit_u64(Deserializer_vec::decode_u64(
			&self.v.value_data[..],
			self.v.value_type,
		)?)
	}

	fn deserialize_bool<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.discard_children_length();
		check_type(self.v.value_type, serde::BOOL)?;
		visitor.visit_bool(Deserializer_vec::decode_bool(
			&self.v.value_data[..],
			self.v.value_type,
		)?)
	}

	fn deserialize_f32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.discard_children_length();
		visitor.visit_f32(Deserializer_vec::decode_f32(
			&self.v.value_data[..],
			self.v.value_type,
		)?)
	}

	fn deserialize_f64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.discard_children_length();
		visitor.visit_f64(Deserializer_vec::decode_f64(
			&self.v.value_data[..],
			self.v.value_type,
		)?)
	}

	fn deserialize_seq<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		let children_length = self.de.read_children_length()?;
		check_type(self.v.value_type, serde::ARR)?;

		visitor.visit_seq(ListDecoder::new(&mut self.de, children_length))
	}

	// Tuples look just like sequences in TT, but the length needs to be checked
	fn deserialize_tuple<V>(self, len: usize, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		check_type(self.v.value_type, serde::ARR)?;
		let children_length = self.de.read_children_length()?;
		if children_length == len {
			visitor.visit_seq(ListDecoder::new(&mut self.de, len))
		} else {
			Err(Error::Message(format!(
				"The length doesnt match: expected {}, got {}",
				len, children_length
			)))
		}
	}

	// Tuple structs look just like sequences in JSON.
	#[inline]
	fn deserialize_tuple_struct<V>(
		self,
		_name: &'static str,
		len: usize,
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_tuple(len, visitor)
	}

	// Much like `deserialize_seq` but calls the visitors `visit_map` method
	// with a `MapAccess` implementation, rather than the visitor's `visit_seq`
	// method with a `SeqAccess` implementation.
	fn deserialize_map<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		check_type(self.v.value_type, serde::MAP)?;
		let children_length = self.de.read_children_length()?;
		visitor.visit_map(MapDecoder::new(
			&mut self.de,
			children_length,
			&mut ValueNode {
				value_data: Vec::new(),
				value_type: Type::from_u8(0),
			},
		))

		// Give the visitor access to each entry of the map.
	}
	// Structs look just like maps in TT.
	fn deserialize_struct<V>(
		self,
		_name: &'static str,
		_fields: &'static [&'static str],
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_map(visitor)
	}
	#[inline]
	fn deserialize_enum<V>(
		self,
		_name: &'static str,
		_variants: &'static [&'static str],
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		check_type(self.v.value_type, serde::MAP)?;
		visitor.visit_enum(Enum::new(&mut self.de))
	}
	/// We never encode Unit values, so we shouldnt decode them either
	#[inline]
	fn deserialize_unit<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		visitor.visit_unit()
	}

	#[inline]
	fn deserialize_unit_struct<V>(
		self,
		_name: &'static str,
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_unit(visitor)
	}

	// Serializers are encouraged to treat newtype structs as
	// insignificant wrappers around the data they contain. That means not
	// parsing anything other than the contained value.
	#[inline]
	fn deserialize_newtype_struct<V>(
		self,
		_name: &'static str,
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		visitor.visit_newtype_struct(self)
	}

	#[inline]
	fn deserialize_identifier<V>(self, _visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		Err(Error::new(
			"TT does not support Deserializer::deserialize_identifier",
		))
	}
	#[inline]
	fn deserialize_ignored_any<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_any(visitor)
	}
}

// `SeqAccess` is provided to the `Visitor` to give it the ability to iterate
// through elements of the sequence.
pub struct ListDecoder<'a, 'de: 'a, R: Read> {
	de: &'a mut Deserializer<'de, R>,
	n: usize,
}

impl<'a, 'de, R: Read> ListDecoder<'a, 'de, R> {
	fn new(de: &'a mut Deserializer<'de, R>, n: usize) -> Self {
		ListDecoder { de, n: n }
	}
}
impl<'de, 'a, R: Read> SeqAccess<'de> for ListDecoder<'a, 'de, R> {
	type Error = Error;

	fn next_element_seed<T>(&mut self, seed: T) -> Result<Option<T::Value>, Self::Error>
	where
		T: DeserializeSeed<'de>,
	{
		if self.n == 0 {
			return Ok(None);
		}
		self.n -= 1;
		// Deserialize an Aay element.
		seed.deserialize(&mut *self.de).map(Some)
	}
	#[inline]
	fn size_hint(&self) -> Option<usize> {
		Some(self.n as usize)
	}
}

// `SeqAccess` is provided to the `Visitor` to give it the ability to iterate
// through elements of the sequence.
pub struct MapDecoder<'a, 'de: 'a, R: Read> {
	de: &'a mut Deserializer<'de, R>,
	n: usize,
	v: &'a mut ValueNode,
}
impl<'a, 'de, R: Read> MapDecoder<'a, 'de, R> {
	fn new(de: &'a mut Deserializer<'de, R>, n: usize, v: &'a mut ValueNode) -> Self {
		MapDecoder { de, n: n, v: v }
	}
}

// `MapAccess` is provided to the `Visitor` to give it the ability to iterate
// through entries of the map.
impl<'de, 'a, R: Read> MapAccess<'de> for MapDecoder<'a, 'de, R> {
	type Error = Error;

	fn next_key_seed<K>(&mut self, seed: K) -> Result<Option<K::Value>, Self::Error>
	where
		K: DeserializeSeed<'de>,
	{
		if self.n == 0 {
			return Ok(None);
		}
		self.n -= 1;
		let mut kser = KeyDeserializer {
			de: self.de,
			v: self.v,
		};
		let res = seed.deserialize(&mut kser).map(Some);
		res
	}

	fn next_value_seed<V>(&mut self, seed: V) -> Result<V::Value, Self::Error>
	where
		V: DeserializeSeed<'de>,
	{
		let mut vser = MapValueDeserializer {
			de: self.de,
			v: self.v,
		};
		let res = seed.deserialize(&mut vser);
		self.v.value_data.clear();
		res
	}
	fn next_entry_seed<K, V>(
		&mut self,
		kseed: K,
		vseed: V,
	) -> Result<Option<(K::Value, V::Value)>, Self::Error>
	where
		K: DeserializeSeed<'de>,
		V: DeserializeSeed<'de>,
	{
		//TODO: maybe optimize this version to not store the value_data in a String but read value first then the key
		if self.n == 0 {
			return Ok(None);
		}
		self.n -= 1;
		let mut kser = KeyDeserializer {
			de: self.de,
			v: self.v,
		};
		let key = kseed.deserialize(&mut kser);
		let mut vser = MapValueDeserializer {
			de: self.de,
			v: self.v,
		};
		let value = vseed.deserialize(&mut vser);
		self.v.value_data.clear();
		Ok(Some((key?, value?)))
	}
	#[inline]
	fn size_hint(&self) -> Option<usize> {
		Some(self.n as usize)
	}
}
fn check_type(value_type: Type, expected_type: Type) -> Result<(), Error> {
	if value_type != expected_type {
		Err(Error::Expected((expected_type, value_type)))
	} else {
		Ok(())
	}
}

struct Enum<'a, 'de: 'a, R: Read> {
	de: &'a mut Deserializer<'de, R>,
	v: ValueNode,
}

impl<'a, 'de, R: Read> Enum<'a, 'de, R> {
	fn new(de: &'a mut Deserializer<'de, R>) -> Self {
		Enum {
			de: de,
			v: ValueNode {
				value_data: Vec::new(),
				value_type: Type::from_u8(0),
			},
		}
	}
}

// `EnumAccess` is provided to the `Visitor` to give it the ability to determine
// which variant of the enum is supposed to be deserialized.
//
// Note that all enum deserialization methods in Serde refer exclusively to the
// "externally tagged" enum representation.
impl<'de, 'a, R: Read> EnumAccess<'de> for Enum<'a, 'de, R> {
	type Error = Error;
	type Variant = Self;

	fn variant_seed<V>(mut self, seed: V) -> Result<(V::Value, Self::Variant), Self::Error>
	where
		V: DeserializeSeed<'de>,
	{
		let mut kser = KeyDeserializer {
			de: self.de,
			v: &mut self.v,
		};
		let val = seed.deserialize(&mut kser);
		Ok((val?, self))
	}
}

// `VariantAccess` is provided to the `Visitor` to give it the ability to see
// the content of the single variant that it decided to deserialize.
impl<'de, 'a, R: Read> VariantAccess<'de> for Enum<'a, 'de, R> {
	type Error = Error;

	// If the `Visitor` expected this variant to be a unit variant, the input
	// should have been the plain string case handled in `deserialize_enum`.
	fn unit_variant(self) -> Result<(), Self::Error> {
		de::Deserialize::deserialize(self.de)
	}

	// Newtype variants are represented in JSON as `{ NAME: VALUE }` so
	// deserialize the value here.
	fn newtype_variant_seed<T>(mut self, seed: T) -> Result<T::Value, Self::Error>
	where
		T: DeserializeSeed<'de>,
	{
		let mut vser = MapValueDeserializer {
			de: self.de,
			v: &mut self.v,
		};

		seed.deserialize(&mut vser)
	}

	// Tuple variants are represented in JSON as `{ NAME: [DATA...] }` so
	// deserialize the sequence of data here.
	fn tuple_variant<V>(mut self, len: usize, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		let mut vser = MapValueDeserializer {
			de: self.de,
			v: &mut self.v,
		};

		de::Deserializer::deserialize_tuple(&mut vser, len, visitor)
	}

	// Struct variants are represented in JSON as `{ NAME: { K: V, ... } }` so
	// deserialize the inner map here.
	fn struct_variant<V>(
		mut self,
		_fields: &'static [&'static str],
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		let mut vser = MapValueDeserializer {
			de: self.de,
			v: &mut self.v,
		};

		de::Deserializer::deserialize_map(&mut vser, visitor)
	}
}
