use crate::ttv3::error::Error;
use crate::ttv3::serde;
use crate::ttv3::serde::Type;
use crate::ttv3::Node;
use ::serde::de::{
	self, DeserializeSeed, EnumAccess, MapAccess, SeqAccess, VariantAccess, Visitor,
};

pub struct Deserializer<'de> {
	// This string starts with the input data and characters are truncated off
	// the beginning as data is parsed.
	pub input: &'de [u8],
	value: Node<'de>,
	read: usize,
	last_read: usize,
}

impl<'de> Deserializer<'de> {
	fn read_node(&mut self) -> Result<(), Error> {
		self.last_read = self.value.from_bytes(&self.input[self.read..])?;
		self.read += self.last_read;
		Ok(())
	}

	// By convention, `Deserializer` constructors are named like `from_xyz`.
	// That way basic use cases are satisfied by something like
	// `serde_json::from_str(...)` while advanced use cases that require a
	// deserializer can make one with `serde_json::Deserializer::from_str(...)`.
	pub fn from_bytes(input: &'de [u8]) -> Self {
		Deserializer {
			input: input,
			value: Node::default(),
			read: 0,
			last_read: 0,
		}
	}
	#[inline]
	pub fn decode_str(bytes: &[u8], typ: Type) -> Result<&str, Error> {
		if typ != serde::STRING && typ != serde::BYTES {
			Err(Error::Expected((serde::STRING, typ)))
		} else {
			Ok(std::str::from_utf8(bytes)?)
		}
	}
	pub fn decode_bytes(bytes: &[u8], typ: Type) -> Result<&[u8], Error> {
		if typ != serde::STRING && typ != serde::BYTES {
			Err(Error::Expected((serde::BYTES, typ)))
		} else {
			Ok(bytes)
		}
	}
	pub fn decode_i8(bytes: &[u8], typ: Type) -> Result<i8, Error> {
		check_type(typ, serde::I8)?;

		let mut buf: [u8; 1] = Default::default();
		buf.clone_from_slice(bytes);

		Ok(i8::from_le_bytes(buf))
	}
	pub fn decode_i16(bytes: &[u8], typ: Type) -> Result<i16, Error> {
		check_type(typ, serde::I16)?;

		let mut buf: [u8; 2] = Default::default();
		buf.clone_from_slice(bytes);

		Ok(i16::from_le_bytes(buf))
	}
	pub fn decode_i32(bytes: &[u8], typ: Type) -> Result<i32, Error> {
		check_type(typ, serde::I32)?;

		let mut buf: [u8; 4] = Default::default();
		buf.clone_from_slice(bytes);

		Ok(i32::from_le_bytes(buf))
	}
	pub fn decode_i64(bytes: &[u8], typ: Type) -> Result<i64, Error> {
		check_type(typ, serde::I64)?;

		let mut buf: [u8; 8] = Default::default();
		buf.clone_from_slice(bytes);

		Ok(i64::from_le_bytes(buf))
	}
	pub fn decode_u8(bytes: &[u8], typ: Type) -> Result<u8, Error> {
		check_type(typ, serde::U8)?;

		let mut buf: [u8; 1] = Default::default();
		buf.clone_from_slice(bytes);

		Ok(u8::from_le_bytes(buf))
	}
	pub fn decode_u16(bytes: &[u8], typ: Type) -> Result<u16, Error> {
		check_type(typ, serde::U16)?;

		let mut buf: [u8; 2] = Default::default();
		buf.clone_from_slice(bytes);

		Ok(u16::from_le_bytes(buf))
	}
	pub fn decode_u32(bytes: &[u8], typ: Type) -> Result<u32, Error> {
		check_type(typ, serde::U32)?;

		let mut buf: [u8; 4] = Default::default();
		buf.clone_from_slice(bytes);

		Ok(u32::from_le_bytes(buf))
	}
	pub fn decode_u64(bytes: &[u8], typ: Type) -> Result<u64, Error> {
		check_type(typ, serde::U64)?;

		let mut buf: [u8; 8] = Default::default();
		buf.clone_from_slice(bytes);

		Ok(u64::from_le_bytes(buf))
	}

	pub fn decode_bool(bytes: &[u8], typ: Type) -> Result<bool, Error> {
		check_type(typ, serde::BOOL)?;
		Ok(bytes[0] == 1)
	}

	pub fn decode_f32(bytes: &[u8], typ: Type) -> Result<f32, Error> {
		check_type(typ, serde::F32)?;

		let mut buf: [u8; 4] = Default::default();
		buf.clone_from_slice(bytes);

		Ok(f32::from_bits(u32::from_le_bytes(buf)))
	}
	pub fn decode_f64(bytes: &[u8], typ: Type) -> Result<f64, Error> {
		check_type(typ, serde::F64)?;

		let mut buf: [u8; 8] = Default::default();
		buf.clone_from_slice(bytes);

		Ok(f64::from_bits(u64::from_le_bytes(buf)))
	}
}

impl<'de, 'a> de::Deserializer<'de> for &'a mut Deserializer<'de> {
	type Error = Error;
	fn deserialize_any<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.read_node()?;
		match self.value.value_type {
			serde::STRING => visitor.visit_borrowed_str(Deserializer::decode_str(
				self.value.value_data,
				self.value.value_type,
			)?),
			serde::BYTES => visitor.visit_bytes(Deserializer::decode_bytes(
				self.value.value_data,
				self.value.value_type,
			)?),
			serde::I8 => visitor.visit_i8(Deserializer::decode_i8(
				self.value.value_data,
				self.value.value_type,
			)?),
			serde::I16 => visitor.visit_i16(Deserializer::decode_i16(
				self.value.value_data,
				self.value.value_type,
			)?),
			serde::I32 => visitor.visit_i32(Deserializer::decode_i32(
				self.value.value_data,
				self.value.value_type,
			)?),
			serde::I64 => visitor.visit_i64(Deserializer::decode_i64(
				self.value.value_data,
				self.value.value_type,
			)?),
			serde::U8 => visitor.visit_u8(Deserializer::decode_u8(
				self.value.value_data,
				self.value.value_type,
			)?),
			serde::U16 => visitor.visit_u16(Deserializer::decode_u16(
				self.value.value_data,
				self.value.value_type,
			)?),
			serde::U32 => visitor.visit_u32(Deserializer::decode_u32(
				self.value.value_data,
				self.value.value_type,
			)?),
			serde::U64 => visitor.visit_u64(Deserializer::decode_u64(
				self.value.value_data,
				self.value.value_type,
			)?),
			serde::BOOL => visitor.visit_bool(Deserializer::decode_bool(
				self.value.value_data,
				self.value.value_type,
			)?),
			serde::F32 => visitor.visit_f32(Deserializer::decode_f32(
				self.value.value_data,
				self.value.value_type,
			)?),
			serde::F64 => visitor.visit_f64(Deserializer::decode_f64(
				self.value.value_data,
				self.value.value_type,
			)?),
			serde::MAP => {
				// Give the visitor access to each entry of the map.
				visitor.visit_map(MapDecoder::new(self, self.value.children_length))
			}
			serde::ARR => {
				let len = self.value.children_length;
				check_type(self.value.value_type, serde::ARR)?;
				visitor.visit_seq(ListDecoder::new(self, len))
			}
			_ => Err(Error::new("Cannot decode Invalid type")),
		}
	}
	fn deserialize_str<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.read_node()?;
		visitor.visit_borrowed_str(Deserializer::decode_str(
			self.value.value_data,
			self.value.value_type,
		)?)
	}

	#[inline]
	fn deserialize_char<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.deserialize_str(visitor)
	}

	#[inline]
	fn deserialize_string<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.deserialize_str(visitor)
	}

	fn deserialize_bytes<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.read_node()?;
		visitor.visit_bytes(Deserializer::decode_bytes(
			self.value.value_data,
			self.value.value_type,
		)?)
	}
	#[inline]
	fn deserialize_byte_buf<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.deserialize_bytes(visitor)
	}

	/// There are no "null" values in TT, so None is not an option.
	#[inline]
	fn deserialize_option<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		visitor.visit_some(self)
	}

	fn deserialize_i8<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.read_node()?;
		visitor.visit_i8(Deserializer::decode_i8(
			self.value.value_data,
			self.value.value_type,
		)?)
	}

	fn deserialize_i16<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.read_node()?;
		visitor.visit_i16(Deserializer::decode_i16(
			self.value.value_data,
			self.value.value_type,
		)?)
	}

	fn deserialize_i32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.read_node()?;
		visitor.visit_i32(Deserializer::decode_i32(
			self.value.value_data,
			self.value.value_type,
		)?)
	}

	fn deserialize_i64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.read_node()?;
		visitor.visit_i64(Deserializer::decode_i64(
			self.value.value_data,
			self.value.value_type,
		)?)
	}

	fn deserialize_u8<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.read_node()?;
		visitor.visit_u8(Deserializer::decode_u8(
			self.value.value_data,
			self.value.value_type,
		)?)
	}

	fn deserialize_u16<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.read_node()?;
		visitor.visit_u16(Deserializer::decode_u16(
			self.value.value_data,
			self.value.value_type,
		)?)
	}

	fn deserialize_u32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.read_node()?;
		visitor.visit_u32(Deserializer::decode_u32(
			self.value.value_data,
			self.value.value_type,
		)?)
	}

	fn deserialize_u64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.read_node()?;
		visitor.visit_u64(Deserializer::decode_u64(
			self.value.value_data,
			self.value.value_type,
		)?)
	}

	fn deserialize_bool<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.read_node()?;
		visitor.visit_bool(Deserializer::decode_bool(
			self.value.value_data,
			self.value.value_type,
		)?)
	}

	fn deserialize_f32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.read_node()?;
		visitor.visit_f32(Deserializer::decode_f32(
			self.value.value_data,
			self.value.value_type,
		)?)
	}

	fn deserialize_f64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.read_node()?;
		visitor.visit_f64(Deserializer::decode_f64(
			self.value.value_data,
			self.value.value_type,
		)?)
	}

	fn deserialize_seq<V>(mut self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.read_node()?;
		let len = self.value.children_length;
		check_type(self.value.value_type, serde::ARR)?;

		visitor.visit_seq(ListDecoder::new(&mut self, len))
	}

	// Tuples look just like sequences in TT, but the length needs to be checked
	fn deserialize_tuple<V>(mut self, len: usize, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.read_node()?;
		check_type(self.value.value_type, serde::ARR)?;
		if self.value.children_length == len {
			visitor.visit_seq(ListDecoder::new(&mut self, len))
		} else {
			Err(Error::Message(format!(
				"The length doesnt match: expected {}, got {}",
				len, self.value.children_length
			)))
		}
	}

	// Tuple structs look just like sequences in JSON.
	#[inline]
	fn deserialize_tuple_struct<V>(
		self,
		_name: &'static str,
		len: usize,
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_tuple(len, visitor)
	}

	// Much like `deserialize_seq` but calls the visitors `visit_map` method
	// with a `MapAccess` implementation, rather than the visitor's `visit_seq`
	// method with a `SeqAccess` implementation.
	fn deserialize_map<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.read_node()?;
		check_type(self.value.value_type, serde::MAP)?;
		visitor.visit_map(MapDecoder::new(self, self.value.children_length))
	}

	// Structs look just like maps in TT.
	#[inline]
	fn deserialize_struct<V>(
		self,
		_name: &'static str,
		_fields: &'static [&'static str],
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_map(visitor)
	}

	fn deserialize_enum<V>(
		self,
		_name: &'static str,
		_variants: &'static [&'static str],
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.read_node()?;
		check_type(self.value.value_type, serde::MAP)?;
		visitor.visit_enum(Enum::new(self))
	}

	/// We never encode Unit values, so we shouldnt decode them either
	#[inline]
	fn deserialize_unit<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		visitor.visit_unit()
	}

	#[inline]
	fn deserialize_unit_struct<V>(
		self,
		_name: &'static str,
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.deserialize_unit(visitor)
	}

	// Serializers are encouraged to treat newtype structs as
	// insignificant wrappers around the data they contain. That means not
	// parsing anything other than the contained value.
	#[inline]
	fn deserialize_newtype_struct<V>(
		self,
		_name: &'static str,
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		visitor.visit_newtype_struct(self)
	}

	#[inline]
	fn deserialize_identifier<V>(self, _visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		Err(Error::new(
			"TT does not support Deserializer::deserialize_identifier",
		))
	}
	#[inline]
	fn deserialize_ignored_any<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_any(visitor)
	}
}
//KeyDesirializer is a one-time decoder for a single key
pub struct KeyDeserializer<'a, 'de: 'a> {
	de: &'a mut Deserializer<'de>,
}

impl<'de, 'a> de::Deserializer<'de> for &'a mut KeyDeserializer<'a, 'de> {
	type Error = Error;
	fn deserialize_any<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.read_node()?;
		match self.de.value.key_type {
			serde::STRING => visitor.visit_borrowed_str(Deserializer::decode_str(
				self.de.value.key_data,
				self.de.value.key_type,
			)?),
			serde::BYTES => visitor.visit_bytes(Deserializer::decode_bytes(
				self.de.value.key_data,
				self.de.value.key_type,
			)?),
			serde::I8 => visitor.visit_i8(Deserializer::decode_i8(
				self.de.value.key_data,
				self.de.value.key_type,
			)?),
			serde::I16 => visitor.visit_i16(Deserializer::decode_i16(
				self.de.value.key_data,
				self.de.value.key_type,
			)?),
			serde::I32 => visitor.visit_i32(Deserializer::decode_i32(
				self.de.value.key_data,
				self.de.value.key_type,
			)?),
			serde::I64 => visitor.visit_i64(Deserializer::decode_i64(
				self.de.value.key_data,
				self.de.value.key_type,
			)?),
			serde::U8 => visitor.visit_u8(Deserializer::decode_u8(
				self.de.value.key_data,
				self.de.value.key_type,
			)?),
			serde::U16 => visitor.visit_u16(Deserializer::decode_u16(
				self.de.value.key_data,
				self.de.value.key_type,
			)?),
			serde::U32 => visitor.visit_u32(Deserializer::decode_u32(
				self.de.value.key_data,
				self.de.value.key_type,
			)?),
			serde::U64 => visitor.visit_u64(Deserializer::decode_u64(
				self.de.value.key_data,
				self.de.value.key_type,
			)?),
			serde::BOOL => visitor.visit_bool(Deserializer::decode_bool(
				self.de.value.key_data,
				self.de.value.key_type,
			)?),
			serde::F32 => visitor.visit_f32(Deserializer::decode_f32(
				self.de.value.key_data,
				self.de.value.key_type,
			)?),
			serde::F64 => visitor.visit_f64(Deserializer::decode_f64(
				self.de.value.key_data,
				self.de.value.key_type,
			)?),
			serde::MAP => Err(Error::ImpossibleKey(serde::MAP)),
			serde::ARR => Err(Error::ImpossibleKey(serde::ARR)),
			_ => Err(Error::new("Cannot decode Invalid")),
		}
	}

	fn deserialize_str<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.de.read_node()?;
		visitor.visit_borrowed_str(Deserializer::decode_str(
			self.de.value.key_data,
			self.de.value.key_type,
		)?)
	}

	#[inline]
	fn deserialize_char<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.deserialize_str(visitor)
	}

	#[inline]
	fn deserialize_string<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.deserialize_str(visitor)
	}

	fn deserialize_bytes<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.de.read_node()?;
		visitor.visit_bytes(Deserializer::decode_bytes(
			self.de.value.key_data,
			self.de.value.key_type,
		)?)
	}
	#[inline]
	fn deserialize_byte_buf<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.deserialize_bytes(visitor)
	}

	/// There are no "null" values in TT, so None is not an option.
	#[inline]
	fn deserialize_option<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		visitor.visit_some(self)
	}

	fn deserialize_i8<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.de.read_node()?;
		visitor.visit_i8(Deserializer::decode_i8(
			self.de.value.key_data,
			self.de.value.key_type,
		)?)
	}

	fn deserialize_i16<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.de.read_node()?;
		visitor.visit_i16(Deserializer::decode_i16(
			self.de.value.key_data,
			self.de.value.key_type,
		)?)
	}

	fn deserialize_i32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.de.read_node()?;
		visitor.visit_i32(Deserializer::decode_i32(
			self.de.value.key_data,
			self.de.value.key_type,
		)?)
	}

	fn deserialize_i64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.de.read_node()?;
		visitor.visit_i64(Deserializer::decode_i64(
			self.de.value.key_data,
			self.de.value.key_type,
		)?)
	}

	fn deserialize_u8<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.de.read_node()?;
		visitor.visit_u8(Deserializer::decode_u8(
			self.de.value.key_data,
			self.de.value.key_type,
		)?)
	}

	fn deserialize_u16<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.de.read_node()?;
		visitor.visit_u16(Deserializer::decode_u16(
			self.de.value.key_data,
			self.de.value.key_type,
		)?)
	}

	fn deserialize_u32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.de.read_node()?;
		visitor.visit_u32(Deserializer::decode_u32(
			self.de.value.key_data,
			self.de.value.key_type,
		)?)
	}

	fn deserialize_u64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.de.read_node()?;
		visitor.visit_u64(Deserializer::decode_u64(
			self.de.value.key_data,
			self.de.value.key_type,
		)?)
	}

	fn deserialize_bool<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.de.read_node()?;
		visitor.visit_bool(Deserializer::decode_bool(
			self.de.value.key_data,
			self.de.value.key_type,
		)?)
	}
	fn deserialize_f32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.de.read_node()?;
		visitor.visit_f32(Deserializer::decode_f32(
			self.de.value.key_data,
			self.de.value.key_type,
		)?)
	}

	fn deserialize_f64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.de.read_node()?;
		visitor.visit_f64(Deserializer::decode_f64(
			self.de.value.key_data,
			self.de.value.key_type,
		)?)
	}

	#[inline]
	fn deserialize_seq<V>(self, _visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		Err(Error::ImpossibleKey(serde::ARR))
	}

	// Tuples look just like sequences in TT, but the length needs to be checked
	#[inline]
	fn deserialize_tuple<V>(self, _len: usize, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_seq(visitor)
	}
	// Tuple structs look just like sequences in JSON.
	#[inline]
	fn deserialize_tuple_struct<V>(
		self,
		_name: &'static str,
		_len: usize,
		visitor: V,
	) -> Result<V::Value, Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_seq(visitor)
	}
	/// We never encode Unit values, so we shouldnt decode them either
	#[inline]
	fn deserialize_unit<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		visitor.visit_unit()
	}

	#[inline]
	fn deserialize_unit_struct<V>(
		self,
		_name: &'static str,
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.deserialize_unit(visitor)
	}

	// Serializers are encouraged to treat newtype structs as
	// insignificant wrappers around the data they contain. That means not
	// parsing anything other than the contained value.
	#[inline]
	fn deserialize_newtype_struct<V>(
		self,
		_name: &'static str,
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		visitor.visit_newtype_struct(self)
	}
	// Much like `deserialize_seq` but calls the visitors `visit_map` method
	// with a `MapAccess` implementation, rather than the visitor's `visit_seq`
	// method with a `SeqAccess` implementation.
	fn deserialize_map<V>(self, _visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		Err(Error::ImpossibleKey(serde::MAP))
	}
	// Structs look just like maps in TT.
	#[inline]
	fn deserialize_struct<V>(
		self,
		_name: &'static str,
		_fields: &'static [&'static str],
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_map(visitor)
	}
	#[inline]
	fn deserialize_enum<V>(
		self,
		_name: &'static str,
		_variants: &'static [&'static str],
		_visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		Err(Error::new("Cannot decode Enum as key"))
	}

	#[inline]
	fn deserialize_identifier<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		self.deserialize_any(visitor)
	}

	#[inline]
	fn deserialize_ignored_any<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_any(visitor)
	}
}

//MapValueDeserializer is a one-time decoder for a single value in a Map type
pub struct MapValueDeserializer<'a, 'de: 'a> {
	de: &'a mut Deserializer<'de>,
}

impl<'de, 'a> de::Deserializer<'de> for &'a mut MapValueDeserializer<'a, 'de> {
	type Error = Error;
	fn deserialize_any<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		match self.de.value.value_type {
			serde::STRING => visitor.visit_borrowed_str(Deserializer::decode_str(
				self.de.value.value_data,
				self.de.value.value_type,
			)?),
			serde::BYTES => visitor.visit_bytes(Deserializer::decode_bytes(
				self.de.value.value_data,
				self.de.value.value_type,
			)?),
			serde::I8 => visitor.visit_i8(Deserializer::decode_i8(
				self.de.value.value_data,
				self.de.value.value_type,
			)?),
			serde::I16 => visitor.visit_i16(Deserializer::decode_i16(
				self.de.value.value_data,
				self.de.value.value_type,
			)?),
			serde::I32 => visitor.visit_i32(Deserializer::decode_i32(
				self.de.value.value_data,
				self.de.value.value_type,
			)?),
			serde::I64 => visitor.visit_i64(Deserializer::decode_i64(
				self.de.value.value_data,
				self.de.value.value_type,
			)?),
			serde::U8 => visitor.visit_u8(Deserializer::decode_u8(
				self.de.value.value_data,
				self.de.value.value_type,
			)?),
			serde::U16 => visitor.visit_u16(Deserializer::decode_u16(
				self.de.value.value_data,
				self.de.value.value_type,
			)?),
			serde::U32 => visitor.visit_u32(Deserializer::decode_u32(
				self.de.value.value_data,
				self.de.value.value_type,
			)?),
			serde::U64 => visitor.visit_u64(Deserializer::decode_u64(
				self.de.value.value_data,
				self.de.value.value_type,
			)?),
			serde::BOOL => visitor.visit_bool(Deserializer::decode_bool(
				self.de.value.value_data,
				self.de.value.value_type,
			)?),
			serde::F32 => visitor.visit_f32(Deserializer::decode_f32(
				self.de.value.value_data,
				self.de.value.value_type,
			)?),
			serde::F64 => visitor.visit_f64(Deserializer::decode_f64(
				self.de.value.value_data,
				self.de.value.value_type,
			)?),
			serde::MAP => {
				// Give the visitor access to each entry of the map.
				visitor.visit_map(MapDecoder::new(self.de, self.de.value.children_length))
			}
			serde::ARR => {
				let len = self.de.value.children_length;
				check_type(self.de.value.value_type, serde::ARR)?;
				visitor.visit_seq(ListDecoder::new(self.de, len))
			}
			_ => Err(Error::new("Cannot decode Invalid type")),
		}
	}
	fn deserialize_str<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		visitor.visit_borrowed_str(Deserializer::decode_str(
			self.de.value.value_data,
			self.de.value.value_type,
		)?)
	}

	#[inline]
	fn deserialize_char<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.deserialize_str(visitor)
	}

	#[inline]
	fn deserialize_string<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.deserialize_str(visitor)
	}

	fn deserialize_bytes<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		visitor.visit_bytes(Deserializer::decode_bytes(
			self.de.value.value_data,
			self.de.value.value_type,
		)?)
	}
	#[inline]
	fn deserialize_byte_buf<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.deserialize_bytes(visitor)
	}

	/// There are no "null" values in TT, so None is not an option.
	#[inline]
	fn deserialize_option<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		visitor.visit_some(self)
	}

	fn deserialize_i8<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		visitor.visit_i8(Deserializer::decode_i8(
			self.de.value.value_data,
			self.de.value.value_type,
		)?)
	}

	fn deserialize_i16<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		visitor.visit_i16(Deserializer::decode_i16(
			self.de.value.value_data,
			self.de.value.value_type,
		)?)
	}

	fn deserialize_i32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		visitor.visit_i32(Deserializer::decode_i32(
			self.de.value.value_data,
			self.de.value.value_type,
		)?)
	}

	fn deserialize_i64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		visitor.visit_i64(Deserializer::decode_i64(
			self.de.value.value_data,
			self.de.value.value_type,
		)?)
	}

	fn deserialize_u8<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		visitor.visit_u8(Deserializer::decode_u8(
			self.de.value.value_data,
			self.de.value.value_type,
		)?)
	}

	fn deserialize_u16<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		visitor.visit_u16(Deserializer::decode_u16(
			self.de.value.value_data,
			self.de.value.value_type,
		)?)
	}

	fn deserialize_u32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		visitor.visit_u32(Deserializer::decode_u32(
			self.de.value.value_data,
			self.de.value.value_type,
		)?)
	}

	fn deserialize_u64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		visitor.visit_u64(Deserializer::decode_u64(
			self.de.value.value_data,
			self.de.value.value_type,
		)?)
	}

	fn deserialize_bool<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		check_type(self.de.value.value_type, serde::BOOL)?;
		visitor.visit_bool(Deserializer::decode_bool(
			self.de.value.value_data,
			self.de.value.value_type,
		)?)
	}

	fn deserialize_f32<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		visitor.visit_f32(Deserializer::decode_f32(
			self.de.value.value_data,
			self.de.value.value_type,
		)?)
	}

	fn deserialize_f64<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		visitor.visit_f64(Deserializer::decode_f64(
			self.de.value.value_data,
			self.de.value.value_type,
		)?)
	}

	fn deserialize_seq<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		let len = self.de.value.children_length;
		check_type(self.de.value.value_type, serde::ARR)?;

		visitor.visit_seq(ListDecoder::new(&mut self.de, len))
	}

	// Tuples look just like sequences in TT, but the length needs to be checked
	fn deserialize_tuple<V>(self, len: usize, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		check_type(self.de.value.value_type, serde::ARR)?;
		if self.de.value.children_length == len {
			visitor.visit_seq(ListDecoder::new(&mut self.de, len))
		} else {
			Err(Error::Message(format!(
				"The length doesnt match: expected {}, got {}",
				len, self.de.value.children_length
			)))
		}
	}

	// Tuple structs look just like sequences in JSON.
	#[inline]
	fn deserialize_tuple_struct<V>(
		self,
		_name: &'static str,
		len: usize,
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_tuple(len, visitor)
	}

	// Much like `deserialize_seq` but calls the visitors `visit_map` method
	// with a `MapAccess` implementation, rather than the visitor's `visit_seq`
	// method with a `SeqAccess` implementation.
	fn deserialize_map<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		check_type(self.de.value.value_type, serde::MAP)?;
		let len = self.de.value.children_length;
		if self.de.value.children_length == len {
			visitor.visit_map(MapDecoder::new(&mut self.de, len))
		} else {
			Err(Error::Message(format!(
				"The length doesnt match: expected {}, got {}",
				len, self.de.value.children_length
			)))
		}

		// Give the visitor access to each entry of the map.
	}
	// Structs look just like maps in TT.
	fn deserialize_struct<V>(
		self,
		_name: &'static str,
		_fields: &'static [&'static str],
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_map(visitor)
	}
	#[inline]
	fn deserialize_enum<V>(
		self,
		_name: &'static str,
		_variants: &'static [&'static str],
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		check_type(self.de.value.value_type, serde::MAP)?;
		visitor.visit_enum(Enum::new(&mut self.de))
	}
	/// We never encode Unit values, so we shouldnt decode them either
	#[inline]
	fn deserialize_unit<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		visitor.visit_unit()
	}

	#[inline]
	fn deserialize_unit_struct<V>(
		self,
		_name: &'static str,
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: de::Visitor<'de>,
	{
		self.deserialize_unit(visitor)
	}

	// Serializers are encouraged to treat newtype structs as
	// insignificant wrappers around the data they contain. That means not
	// parsing anything other than the contained value.
	#[inline]
	fn deserialize_newtype_struct<V>(
		self,
		_name: &'static str,
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		visitor.visit_newtype_struct(self)
	}

	#[inline]
	fn deserialize_identifier<V>(self, _visitor: V) -> Result<V::Value, Self::Error>
	where
		V: serde::de::Visitor<'de>,
	{
		Err(Error::new(
			"TT does not support Deserializer::deserialize_identifier",
		))
	}
	#[inline]
	fn deserialize_ignored_any<V>(self, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		self.deserialize_any(visitor)
	}
}

// `SeqAccess` is provided to the `Visitor` to give it the ability to iterate
// through elements of the sequence.
pub struct ListDecoder<'a, 'de: 'a> {
	de: &'a mut Deserializer<'de>,
	n: usize,
}
impl<'a, 'de> ListDecoder<'a, 'de> {
	fn new(de: &'a mut Deserializer<'de>, n: usize) -> Self {
		ListDecoder { de, n: n }
	}
}
impl<'de, 'a> SeqAccess<'de> for ListDecoder<'a, 'de> {
	type Error = Error;

	fn next_element_seed<T>(&mut self, seed: T) -> Result<Option<T::Value>, Self::Error>
	where
		T: DeserializeSeed<'de>,
	{
		if self.n == 0 {
			return Ok(None);
		}
		self.n -= 1;
		// Deserialize an Aay element.
		seed.deserialize(&mut *self.de).map(Some)
	}
	#[inline]
	fn size_hint(&self) -> Option<usize> {
		Some(self.n as usize)
	}
}

// `SeqAccess` is provided to the `Visitor` to give it the ability to iterate
// through elements of the sequence.
pub struct MapDecoder<'a, 'de: 'a> {
	de: &'a mut Deserializer<'de>,
	n: usize,
}
impl<'a, 'de> MapDecoder<'a, 'de> {
	fn new(de: &'a mut Deserializer<'de>, n: usize) -> Self {
		MapDecoder { de, n: n }
	}
}

// `MapAccess` is provided to the `Visitor` to give it the ability to iterate
// through entries of the map.
impl<'de, 'a> MapAccess<'de> for MapDecoder<'a, 'de> {
	type Error = Error;

	fn next_key_seed<K>(&mut self, seed: K) -> Result<Option<K::Value>, Self::Error>
	where
		K: DeserializeSeed<'de>,
	{
		if self.n == 0 {
			return Ok(None);
		}
		self.n -= 1;

		seed.deserialize(&mut KeyDeserializer { de: self.de })
			.map(Some)
	}

	fn next_value_seed<V>(&mut self, seed: V) -> Result<V::Value, Self::Error>
	where
		V: DeserializeSeed<'de>,
	{
		seed.deserialize(&mut MapValueDeserializer { de: self.de })
	}
	fn next_entry_seed<K, V>(
		&mut self,
		kseed: K,
		vseed: V,
	) -> Result<Option<(K::Value, V::Value)>, Self::Error>
	where
		K: DeserializeSeed<'de>,
		V: DeserializeSeed<'de>,
	{
		if self.n == 0 {
			return Ok(None);
		}
		self.n -= 1;
		// We have to load the node here because the de.value will get overwritten if multiple values are read
		let key = kseed.deserialize(&mut KeyDeserializer { de: self.de });
		let value = vseed.deserialize(&mut MapValueDeserializer { de: self.de });
		Ok(Some((key?, value?)))
	}
	#[inline]
	fn size_hint(&self) -> Option<usize> {
		Some(self.n as usize)
	}
}
fn check_type(value_type: Type, expected_type: Type) -> Result<(), Error> {
	if value_type != expected_type {
		Err(Error::Expected((expected_type, value_type)))
	} else {
		Ok(())
	}
}

struct Enum<'a, 'de: 'a> {
	de: &'a mut Deserializer<'de>,
}

impl<'a, 'de> Enum<'a, 'de> {
	fn new(de: &'a mut Deserializer<'de>) -> Self {
		Enum { de }
	}
}

// `EnumAccess` is provided to the `Visitor` to give it the ability to determine
// which variant of the enum is supposed to be deserialized.
//
// Note that all enum deserialization methods in Serde refer exclusively to the
// "externally tagged" enum representation.
impl<'de, 'a> EnumAccess<'de> for Enum<'a, 'de> {
	type Error = Error;
	type Variant = Self;

	fn variant_seed<V>(self, seed: V) -> Result<(V::Value, Self::Variant), Self::Error>
	where
		V: DeserializeSeed<'de>,
	{
		let val = seed.deserialize(&mut KeyDeserializer { de: self.de });
		Ok((val?, self))
	}
}

// `VariantAccess` is provided to the `Visitor` to give it the ability to see
// the content of the single variant that it decided to deserialize.
impl<'de, 'a> VariantAccess<'de> for Enum<'a, 'de> {
	type Error = Error;

	// If the `Visitor` expected this variant to be a unit variant, the input
	// should have been the plain string case handled in `deserialize_enum`.
	fn unit_variant(self) -> Result<(), Self::Error> {
		de::Deserialize::deserialize(self.de)
	}

	// Newtype variants are represented in JSON as `{ NAME: VALUE }` so
	// deserialize the value here.
	fn newtype_variant_seed<T>(self, seed: T) -> Result<T::Value, Self::Error>
	where
		T: DeserializeSeed<'de>,
	{
		seed.deserialize(&mut MapValueDeserializer { de: self.de })
	}

	// Tuple variants are represented in JSON as `{ NAME: [DATA...] }` so
	// deserialize the sequence of data here.
	fn tuple_variant<V>(self, len: usize, visitor: V) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		de::Deserializer::deserialize_tuple(&mut MapValueDeserializer { de: self.de }, len, visitor)
	}

	// Struct variants are represented in JSON as `{ NAME: { K: V, ... } }` so
	// deserialize the inner map here.
	fn struct_variant<V>(
		self,
		_fields: &'static [&'static str],
		visitor: V,
	) -> Result<V::Value, Self::Error>
	where
		V: Visitor<'de>,
	{
		de::Deserializer::deserialize_map(&mut MapValueDeserializer { de: self.de }, visitor)
	}
}
