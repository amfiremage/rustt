use crate::ttv3::error::Error;
use crate::ttv3::serde;
use crate::ttv3::serde::CustomNode;
use crate::ttv3::serde::Empty;
use crate::ttv3::serde::KeyEncodable;
use crate::ttv3::write_key_value;
use crate::ttv3::write_key_value_buf;
use ::serde::ser::{self, Impossible, Serialize};
use std::io;

pub struct ValueSerializer<'w, W, K> {
	pub writer: &'w mut W,
	key: K,
	varbuf: [u8; 10],
}

impl<'w, W, K> ValueSerializer<'w, W, K>
where
	W: io::Write,
	K: KeyEncodable,
{
	/// Creates a new TT serializer.
	#[inline]
	pub fn new(writer: &'w mut W, key: K) -> Self {
		ValueSerializer {
			writer: writer,
			key: key,
			varbuf: [0; 10],
		}
	}
}

impl<'a, 'w: 'a, W, K> ser::Serializer for &'a mut ValueSerializer<'w, W, K>
where
	W: io::Write,
	K: KeyEncodable,
{
	type Ok = ();
	type Error = Error;

	type SerializeSeq = SeqSerializer<'a, W>;
	type SerializeTuple = SeqSerializer<'a, W>;
	type SerializeTupleStruct = SeqSerializer<'a, W>;
	type SerializeTupleVariant = SeqSerializer<'a, W>;
	type SerializeMap = MapSerializer<'a, W>;
	type SerializeStruct = StructSerializer<'a, W>;
	type SerializeStructVariant = StructSerializer<'a, W>;

	#[inline]
	fn serialize_str(self, value: &str) -> Result<(), Error> {
		write_key_value_buf(&self.key, &value, self.writer, &mut self.varbuf)?;
		Ok(())
	}

	#[inline]
	fn serialize_char(self, value: char) -> Result<(), Error> {
		// A char encoded as UTF-8 takes 4 bytes at most.
		let mut buf = [0; 4];
		self.serialize_str(value.encode_utf8(&mut buf))
	}

	#[inline]
	fn serialize_bytes(self, value: &[u8]) -> Result<(), Error> {
		write_key_value_buf(&self.key, &value, self.writer, &mut self.varbuf)?;
		Ok(())
	}

	#[inline]
	fn serialize_i8(self, value: i8) -> Result<(), Error> {
		write_key_value_buf(&self.key, &value, self.writer, &mut self.varbuf)?;
		Ok(())
	}

	#[inline]
	fn serialize_i16(self, value: i16) -> Result<(), Error> {
		write_key_value_buf(&self.key, &value, self.writer, &mut self.varbuf)?;
		Ok(())
	}

	#[inline]
	fn serialize_i32(self, value: i32) -> Result<(), Error> {
		write_key_value_buf(&self.key, &value, self.writer, &mut self.varbuf)?;
		Ok(())
	}

	#[inline]
	fn serialize_i64(self, value: i64) -> Result<(), Error> {
		write_key_value_buf(&self.key, &value, self.writer, &mut self.varbuf)?;
		Ok(())
	}

	#[inline]
	fn serialize_u8(self, value: u8) -> Result<(), Error> {
		write_key_value_buf(&self.key, &value, self.writer, &mut self.varbuf)?;
		Ok(())
	}

	#[inline]
	fn serialize_u16(self, value: u16) -> Result<(), Error> {
		write_key_value_buf(&self.key, &value, self.writer, &mut self.varbuf)?;
		Ok(())
	}

	#[inline]
	fn serialize_u32(self, value: u32) -> Result<(), Error> {
		write_key_value_buf(&self.key, &value, self.writer, &mut self.varbuf)?;
		Ok(())
	}

	#[inline]
	fn serialize_u64(self, value: u64) -> Result<(), Error> {
		write_key_value_buf(&self.key, &value, self.writer, &mut self.varbuf)?;
		Ok(())
	}

	#[inline]
	fn serialize_f32(self, value: f32) -> Result<(), Error> {
		write_key_value_buf(&self.key, &value, self.writer, &mut self.varbuf)?;
		Ok(())
	}

	#[inline]
	fn serialize_f64(self, value: f64) -> Result<(), Error> {
		write_key_value_buf(&self.key, &value, self.writer, &mut self.varbuf)?;
		Ok(())
	}

	#[inline]
	fn serialize_bool(self, value: bool) -> Result<(), Error> {
		write_key_value_buf(&self.key, &value, self.writer, &mut self.varbuf)?;
		Ok(())
	}

	#[inline]
	fn serialize_newtype_struct<T>(self, _name: &'static str, value: &T) -> Result<(), Error>
	where
		T: ?Sized + Serialize,
	{
		value.serialize(self)
	}

	// Maps are represented in JSON as `{ K: V, K: V, ... }`.
	fn serialize_map(self, len: Option<usize>) -> Result<Self::SerializeMap, Error> {
		match len {
			Some(l) => {
				write_key_value(
					&self.key,
					&serde::CustomStructured {
						ttype: serde::MAP,
						children_length: l,
					},
					self.writer,
				)?;
				Ok(MapSerializer {
					writer: self.writer,
					key: CustomNode {
						data: Vec::new(),
						ttype: serde::Type::from_u8(0),
						children_length: 0,
					},
				})
			}
			None => return Err(Error::NeedsLength()),
		}
	}

	#[inline]
	fn serialize_struct(
		self,
		_name: &'static str,
		len: usize,
	) -> Result<Self::SerializeStruct, Error> {
		write_key_value(
			&self.key,
			&serde::CustomStructured {
				ttype: serde::MAP,
				children_length: len,
			},
			self.writer,
		)?;
		Ok(StructSerializer {
			writer: self.writer,
		})
	}

	#[inline]
	fn serialize_struct_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		variant: &'static str,
		len: usize,
	) -> Result<Self::SerializeStructVariant, Error> {
		write_key_value(
			&self.key,
			&serde::CustomStructured {
				ttype: serde::MAP,
				children_length: 1,
			},
			self.writer,
		)?;
		write_key_value(
			&variant,
			&serde::CustomStructured {
				ttype: serde::MAP,
				children_length: len,
			},
			self.writer,
		)?;

		Ok(StructSerializer {
			writer: self.writer,
		})
	}

	// TT is platform independant and self-describing. If the order of varints is diferent on
	// the reaceiver compred to the sender it wont work thus the name was chosen
	fn serialize_unit_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		variant: &'static str,
	) -> Result<(), Error> {
		write_key_value(
			&self.key,
			&serde::CustomStructured {
				ttype: serde::MAP,
				children_length: 1,
			},
			self.writer,
		)?;
		write_key_value(&variant, &variant, self.writer)?;
		Ok(())
	}
	// We serialize this is a key-value
	fn serialize_newtype_variant<T>(
		self,
		_name: &'static str,
		_variant_index: u32,
		variant: &'static str,
		value: &T,
	) -> Result<(), Error>
	where
		T: ?Sized + Serialize,
	{
		write_key_value(
			&self.key,
			&serde::CustomStructured {
				ttype: serde::MAP,
				children_length: 1,
			},
			self.writer,
		)?;
		value.serialize(&mut ValueSerializer::new(self.writer, variant))
	}
	// Tuple variants are represented in JSON as `{ NAME: [DATA...] }`. Again
	// this method is only responsible for the externally tagged representation.
	fn serialize_tuple_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		variant: &'static str,
		len: usize,
	) -> Result<Self::SerializeTupleVariant, Error> {
		write_key_value(
			&self.key,
			&serde::CustomStructured {
				ttype: serde::MAP,
				children_length: 1,
			},
			self.writer,
		)?;
		write_key_value(
			&variant,
			&serde::CustomStructured {
				ttype: serde::ARR,
				children_length: len,
			},
			self.writer,
		)?;

		Ok(SeqSerializer {
			writer: self.writer,
		})
	}

	fn serialize_tuple(self, len: usize) -> Result<Self::SerializeTuple, Error> {
		write_key_value(
			&self.key,
			&serde::CustomStructured {
				ttype: serde::ARR,
				children_length: len,
			},
			self.writer,
		)?;
		Ok(SeqSerializer {
			writer: self.writer,
		})
	}

	// Tuple structs look just like sequences in JSON.
	#[inline]
	fn serialize_tuple_struct(
		self,
		_name: &'static str,
		len: usize,
	) -> Result<Self::SerializeTupleStruct, Error> {
		self.serialize_tuple(len)
	}

	fn serialize_seq(self, len: Option<usize>) -> Result<Self::SerializeSeq, Error> {
		match len {
			Some(l) => {
				write_key_value(
					&self.key,
					&serde::CustomStructured {
						ttype: serde::ARR,
						children_length: l,
					},
					self.writer,
				)?;
				Ok(SeqSerializer {
					writer: self.writer,
				})
			}
			None => Err(Error::NeedsLength()),
		}
	}

	// In Serde, unit means an anonymous value containing no data. Map this to
	// JSON as `null`.
	#[inline]
	fn serialize_unit(self) -> Result<(), Error> {
		Err(Error::Empty())
	}

	// Unit struct means a named value containing no data. Again, since there is
	// no data, map this to JSON as `null`. There is no need to serialize the
	// name in most formats.
	fn serialize_unit_struct(self, _name: &'static str) -> Result<(), Error> {
		self.serialize_unit()
	}
	// An absent optional is represented as the JSON `null`.
	#[inline]
	fn serialize_none(self) -> Result<(), Error> {
		Err(Error::Empty())
	}

	// A present optional is represented as just the contained value. Note that
	// this is a lossy representation. For example the values `Some(())` and
	// `None` both serialize as just `null`. Unfortunately this is typically
	// what people expect when working with JSON. Other formats are encouraged
	// to behave more intelligently if possible.
	#[inline]
	fn serialize_some<T>(self, value: &T) -> Result<(), Error>
	where
		T: ?Sized + Serialize,
	{
		value.serialize(self)
	}

	fn is_human_readable(&self) -> bool {
		false
	}
}

pub struct SeqSerializer<'s, W> {
	writer: &'s mut W,
}

impl<'s, W> ser::SerializeTupleVariant for SeqSerializer<'s, W>
where
	W: io::Write,
{
	type Ok = ();
	type Error = Error;

	#[inline]
	fn serialize_field<T: ?Sized + Serialize>(&mut self, value: &T) -> Result<(), Error> {
		value.serialize(&mut ValueSerializer::new(self.writer, Empty {}))
	}
	#[inline]
	fn end(self) -> Result<(), Error> {
		Ok(())
	}
}

impl<'s, W> ser::SerializeSeq for SeqSerializer<'s, W>
where
	W: io::Write,
{
	type Ok = ();
	type Error = Error;

	#[inline]
	fn serialize_element<T: ?Sized + Serialize>(&mut self, value: &T) -> Result<(), Error> {
		value.serialize(&mut ValueSerializer::new(self.writer, Empty {}))
	}
	#[inline]
	fn end(self) -> Result<(), Error> {
		Ok(())
	}
}

impl<'s, W> ser::SerializeTuple for SeqSerializer<'s, W>
where
	W: io::Write,
{
	type Ok = ();
	type Error = Error;

	#[inline]
	fn serialize_element<T: ?Sized + Serialize>(&mut self, value: &T) -> Result<(), Error> {
		value.serialize(&mut ValueSerializer::new(self.writer, Empty {}))
	}
	#[inline]
	fn end(self) -> Result<(), Error> {
		Ok(())
	}
}

impl<'s, W> ser::SerializeTupleStruct for SeqSerializer<'s, W>
where
	W: io::Write,
{
	type Ok = ();
	type Error = Error;

	#[inline]
	fn serialize_field<T: ?Sized>(&mut self, value: &T) -> Result<(), Self::Error>
	where
		T: Serialize,
	{
		value.serialize(&mut ValueSerializer::new(self.writer, Empty {}))
	}
	#[inline]
	fn end(self) -> Result<(), Error> {
		Ok(())
	}
}

pub struct MapSerializer<'s, W> {
	writer: &'s mut W,
	key: serde::CustomNode,
}

impl<'a, W> ser::SerializeMap for MapSerializer<'a, W>
where
	W: io::Write,
{
	type Ok = ();
	type Error = Error;

	#[inline]
	fn serialize_key<T>(&mut self, key: &T) -> Result<(), Error>
	where
		T: ?Sized + Serialize,
	{
		self.key = key.serialize(KeySerializer {})?;
		Ok(())
	}

	#[inline]
	fn serialize_value<T>(&mut self, value: &T) -> Result<(), Error>
	where
		T: ?Sized + Serialize,
	{
		value.serialize(&mut ValueSerializer::new(self.writer, &self.key))
	}

	#[inline]
	fn serialize_entry<K: ?Sized, V: ?Sized>(
		&mut self,
		key: &K,
		value: &V,
	) -> Result<(), Self::Error>
	where
		K: Serialize,
		V: Serialize,
	{
		value.serialize(&mut ValueSerializer::new(self.writer, &key))
	}

	#[inline]
	fn end(self) -> Result<(), Error> {
		Ok(())
	}
}

pub struct StructSerializer<'s, W> {
	writer: &'s mut W,
}

impl<'a, W> ser::SerializeStruct for StructSerializer<'a, W>
where
	W: io::Write,
{
	type Ok = ();
	type Error = Error;

	#[inline]
	fn serialize_field<T: ?Sized>(&mut self, key: &'static str, value: &T) -> Result<(), Error>
	where
		T: Serialize,
	{
		value.serialize(&mut ValueSerializer::new(self.writer, key))
	}

	/// Indicate that a struct field has been skipped.
	#[inline]
	fn skip_field(&mut self, _key: &'static str) -> Result<(), Error> {
		Err(Error::new("cannot skip struct fields when serde TT"))
	}

	#[inline]
	fn end(self) -> Result<(), Error> {
		Ok(())
	}
}

impl<'a, W> ser::SerializeStructVariant for StructSerializer<'a, W>
where
	W: io::Write,
{
	type Ok = ();
	type Error = Error;

	#[inline]
	fn serialize_field<T: ?Sized>(&mut self, key: &'static str, value: &T) -> Result<(), Error>
	where
		T: Serialize,
	{
		value.serialize(&mut ValueSerializer::new(self.writer, key))
	}

	/// Indicate that a struct field has been skipped.
	#[inline]
	fn skip_field(&mut self, _key: &'static str) -> Result<(), Error> {
		Err(Error::new("cannot skip struct fields when serde TT"))
	}

	#[inline]
	fn end(self) -> Result<(), Error> {
		Ok(())
	}
}

////////////////////////////////////////////////////////////////////////////////

// This is just for getting key data from a Serialize type
pub struct KeySerializer {}

impl ser::Serializer for KeySerializer {
	type Ok = serde::CustomNode;
	type Error = Error;

	type SerializeSeq = Impossible<serde::CustomNode, Error>;
	type SerializeTuple = Impossible<serde::CustomNode, Error>;
	type SerializeTupleStruct = Impossible<serde::CustomNode, Error>;
	type SerializeTupleVariant = Impossible<serde::CustomNode, Error>;
	type SerializeMap = Impossible<serde::CustomNode, Error>;
	type SerializeStruct = Impossible<serde::CustomNode, Error>;
	type SerializeStructVariant = Impossible<serde::CustomNode, Error>;

	#[inline]
	fn serialize_str(self, value: &str) -> Result<CustomNode, Error> {
		Ok(CustomNode {
			data: value.as_bytes().to_vec(),
			ttype: serde::STRING,
			children_length: 0,
		})
	}

	#[inline]
	fn serialize_char(self, value: char) -> Result<CustomNode, Error> {
		// A char encoded as UTF-8 takes 4 bytes at most.
		let mut buf = [0; 4];
		self.serialize_str(value.encode_utf8(&mut buf))
	}

	#[inline]
	fn serialize_bytes(self, value: &[u8]) -> Result<CustomNode, Error> {
		Ok(CustomNode {
			data: value.to_vec(),
			ttype: serde::BYTES,
			children_length: 0,
		})
	}
	#[inline]
	fn serialize_i8(self, value: i8) -> Result<CustomNode, Error> {
		Ok(CustomNode {
			data: serde::encode_i8(value).to_vec(),
			ttype: serde::I8,
			children_length: 0,
		})
	}
	#[inline]
	fn serialize_i16(self, value: i16) -> Result<CustomNode, Error> {
		Ok(CustomNode {
			data: serde::encode_i16(value).to_vec(),
			ttype: serde::I16,
			children_length: 0,
		})
	}
	#[inline]
	fn serialize_i32(self, value: i32) -> Result<CustomNode, Error> {
		Ok(CustomNode {
			data: serde::encode_i32(value).to_vec(),
			ttype: serde::I32,
			children_length: 0,
		})
	}
	#[inline]
	fn serialize_i64(self, value: i64) -> Result<CustomNode, Error> {
		Ok(CustomNode {
			data: serde::encode_i64(value).to_vec(),
			ttype: serde::I64,
			children_length: 0,
		})
	}
	#[inline]
	fn serialize_u8(self, value: u8) -> Result<CustomNode, Error> {
		Ok(CustomNode {
			data: serde::encode_u8(value).to_vec(),
			ttype: serde::U8,
			children_length: 0,
		})
	}
	#[inline]
	fn serialize_u16(self, value: u16) -> Result<CustomNode, Error> {
		Ok(CustomNode {
			data: serde::encode_u16(value).to_vec(),
			ttype: serde::U16,
			children_length: 0,
		})
	}
	#[inline]
	fn serialize_u32(self, value: u32) -> Result<CustomNode, Error> {
		Ok(CustomNode {
			data: serde::encode_u32(value).to_vec(),
			ttype: serde::U32,
			children_length: 0,
		})
	}
	#[inline]
	fn serialize_u64(self, value: u64) -> Result<CustomNode, Error> {
		Ok(CustomNode {
			data: serde::encode_u64(value).to_vec(),
			ttype: serde::U64,
			children_length: 0,
		})
	}
	#[inline]
	fn serialize_f32(self, value: f32) -> Result<CustomNode, Error> {
		Ok(CustomNode {
			data: serde::encode_f32(value).to_vec(),
			ttype: serde::F32,
			children_length: 0,
		})
	}
	#[inline]
	fn serialize_f64(self, value: f64) -> Result<CustomNode, Error> {
		Ok(CustomNode {
			data: serde::encode_f64(value).to_vec(),
			ttype: serde::F64,
			children_length: 0,
		})
	}
	#[inline]
	fn serialize_bool(self, value: bool) -> Result<CustomNode, Error> {
		Ok(CustomNode {
			data: match value {
				true => vec![1],
				false => vec![0],
			},
			ttype: serde::BOOL,
			children_length: 0,
		})
	}

	fn serialize_newtype_struct<T>(
		self,
		_name: &'static str,
		_value: &T,
	) -> Result<CustomNode, Error>
	where
		T: ?Sized + Serialize,
	{
		Err(Error::ImpossibleKey(serde::MAP))
	}

	// TT usually doesnt encode enums into keys, but simple enums can be described
	// using just a string so we well use that.
	fn serialize_unit_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		variant: &'static str,
	) -> Result<CustomNode, Error> {
		self.serialize_str(variant)
	}

	// We serialize this is a key-value
	fn serialize_newtype_variant<T>(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		_value: &T,
	) -> Result<CustomNode, Error>
	where
		T: ?Sized + Serialize,
	{
		Err(Error::new(
			"Cannot encode enums with data attached as keys, TT can only encode basic types as key",
		))
	}

	// In Serde, unit means an anonymous value containing no data. Map this to
	// JSON as `null`.
	#[inline]
	fn serialize_unit(self) -> Result<CustomNode, Error> {
		Err(Error::Empty())
	}

	// Unit struct means a named value containing no data. Again, since there is
	// no data, map this to JSON as `null`. There is no need to serialize the
	// name in most formats.
	fn serialize_unit_struct(self, _name: &'static str) -> Result<CustomNode, Error> {
		self.serialize_unit()
	}

	fn serialize_seq(self, _len: Option<usize>) -> Result<Self::SerializeSeq, Error> {
		Err(Error::ImpossibleKey(serde::ARR))
	}

	fn serialize_tuple(self, len: usize) -> Result<Self::SerializeTuple, Error> {
		self.serialize_seq(Some(len))
	}

	// Tuple structs look just like sequences in JSON.
	fn serialize_tuple_struct(
		self,
		_name: &'static str,
		_len: usize,
	) -> Result<Self::SerializeTupleStruct, Error> {
		Err(Error::ImpossibleKey(serde::ARR))
	}

	fn serialize_struct(
		self,
		_name: &'static str,
		_len: usize,
	) -> Result<Self::SerializeStruct, Error> {
		Err(Error::ImpossibleKey(serde::MAP))
	}

	// Maps are represented in JSON as `{ K: V, K: V, ... }`.
	fn serialize_map(self, _len: Option<usize>) -> Result<Self::SerializeMap, Error> {
		Err(Error::ImpossibleKey(serde::MAP))
	}

	fn serialize_struct_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		_len: usize,
	) -> Result<Self::SerializeStructVariant, Error> {
		Err(Error::new(
			"Cannot encode enums with data attached as keys, TT can only encode basic types as key",
		))
	}

	// Tuple variants are represented in JSON as `{ NAME: [DATA...] }`. Again
	// this method is only responsible for the externally tagged representation.
	fn serialize_tuple_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		_len: usize,
	) -> Result<Self::SerializeTupleVariant, Error> {
		Err(Error::new(
			"Cannot encode enums with data attached as keys, TT can only encode basic types as key",
		))
	}

	#[inline]
	fn serialize_none(self) -> Result<CustomNode, Error> {
		Err(Error::Empty())
	}

	// A present optional is represented as just the contained value. Note that
	// this is a lossy representation. For example the values `Some(())` and
	// `None` both serialize as just `null`. Unfortunately this is typically
	// what people expect when working with JSON. Other formats are encouraged
	// to behave more intelligently if possible.
	#[inline]
	fn serialize_some<T>(self, value: &T) -> Result<CustomNode, Error>
	where
		T: ?Sized + Serialize,
	{
		value.serialize(self)
	}

	fn is_human_readable(&self) -> bool {
		false
	}
}
pub struct KeyWriter<'w, W: io::Write> {
	pub writer: &'w mut W,
}

impl<'w, W> ser::Serializer for KeyWriter<'w, W>
where
	W: io::Write,
{
	type Ok = ();
	type Error = Error;

	type SerializeSeq = Impossible<(), Error>;
	type SerializeTuple = Impossible<(), Error>;
	type SerializeTupleStruct = Impossible<(), Error>;
	type SerializeTupleVariant = Impossible<(), Error>;
	type SerializeMap = Impossible<(), Error>;
	type SerializeStruct = Impossible<(), Error>;
	type SerializeStructVariant = Impossible<(), Error>;

	#[inline]
	fn serialize_str(self, value: &str) -> Result<(), Error> {
		value.write_bytes(self.writer)?;
		Ok(())
	}

	#[inline]
	fn serialize_char(self, value: char) -> Result<(), Error> {
		// A char encoded as UTF-8 takes 4 bytes at most.
		let mut buf = [0; 4];
		self.serialize_str(value.encode_utf8(&mut buf))
	}

	#[inline]
	fn serialize_bytes(self, value: &[u8]) -> Result<(), Error> {
		self.writer.write_all(value)?;
		Ok(())
	}
	#[inline]
	fn serialize_i8(self, value: i8) -> Result<(), Error> {
		value.write_bytes(self.writer)?;
		Ok(())
	}
	#[inline]
	fn serialize_i16(self, value: i16) -> Result<(), Error> {
		value.write_bytes(self.writer)?;
		Ok(())
	}
	#[inline]
	fn serialize_i32(self, value: i32) -> Result<(), Error> {
		value.write_bytes(self.writer)?;
		Ok(())
	}
	#[inline]
	fn serialize_i64(self, value: i64) -> Result<(), Error> {
		value.write_bytes(self.writer)?;
		Ok(())
	}
	#[inline]
	fn serialize_u8(self, value: u8) -> Result<(), Error> {
		value.write_bytes(self.writer)?;
		Ok(())
	}
	#[inline]
	fn serialize_u16(self, value: u16) -> Result<(), Error> {
		value.write_bytes(self.writer)?;
		Ok(())
	}
	#[inline]
	fn serialize_u32(self, value: u32) -> Result<(), Error> {
		value.write_bytes(self.writer)?;
		Ok(())
	}
	#[inline]
	fn serialize_u64(self, value: u64) -> Result<(), Error> {
		value.write_bytes(self.writer)?;
		Ok(())
	}
	#[inline]
	fn serialize_f32(self, value: f32) -> Result<(), Error> {
		value.write_bytes(self.writer)?;
		Ok(())
	}
	#[inline]
	fn serialize_f64(self, value: f64) -> Result<(), Error> {
		value.write_bytes(self.writer)?;
		Ok(())
	}
	#[inline]
	fn serialize_bool(self, value: bool) -> Result<(), Error> {
		value.write_bytes(self.writer)?;
		Ok(())
	}

	// TT usually doesnt encode enums into keys, but simple enums can be described
	// using just a string so we well use that.
	fn serialize_unit_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		variant: &'static str,
	) -> Result<(), Error> {
		self.serialize_str(variant)
	}

	fn serialize_newtype_struct<T>(self, _name: &'static str, _value: &T) -> Result<(), Error>
	where
		T: ?Sized + Serialize,
	{
		Err(Error::ImpossibleKey(serde::MAP))
	}

	// We serialize this is a key-value
	fn serialize_newtype_variant<T>(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		_value: &T,
	) -> Result<(), Error>
	where
		T: ?Sized + Serialize,
	{
		Err(Error::new(
			"Cannot encode enums with data attached as keys, TT can only encode basic types as key",
		))
	}

	// In Serde, unit means an anonymous value containing no data. Map this to
	// JSON as `null`.
	#[inline]
	fn serialize_unit(self) -> Result<(), Error> {
		Err(Error::Empty())
	}

	// Unit struct means a named value containing no data. Again, since there is
	// no data, map this to JSON as `null`. There is no need to serialize the
	// name in most formats.
	fn serialize_unit_struct(self, _name: &'static str) -> Result<(), Error> {
		self.serialize_unit()
	}

	fn serialize_seq(self, _len: Option<usize>) -> Result<Self::SerializeSeq, Error> {
		Err(Error::ImpossibleKey(serde::ARR))
	}

	fn serialize_tuple(self, len: usize) -> Result<Self::SerializeTuple, Error> {
		self.serialize_seq(Some(len))
	}

	// Tuple structs look just like sequences in JSON.
	fn serialize_tuple_struct(
		self,
		_name: &'static str,
		_len: usize,
	) -> Result<Self::SerializeTupleStruct, Error> {
		Err(Error::ImpossibleKey(serde::ARR))
	}

	fn serialize_struct(
		self,
		_name: &'static str,
		_len: usize,
	) -> Result<Self::SerializeStruct, Error> {
		Err(Error::ImpossibleKey(serde::MAP))
	}

	fn serialize_struct_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		_len: usize,
	) -> Result<Self::SerializeStructVariant, Error> {
		Err(Error::new(
			"Cannot encode enums with data attached as keys, TT can only encode basic types as key",
		))
	}

	// Maps are represented in JSON as `{ K: V, K: V, ... }`.
	fn serialize_map(self, _len: Option<usize>) -> Result<Self::SerializeMap, Error> {
		Err(Error::ImpossibleKey(serde::MAP))
	}

	// Tuple variants are represented in JSON as `{ NAME: [DATA...] }`. Again
	// this method is only responsible for the externally tagged representation.
	fn serialize_tuple_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		_len: usize,
	) -> Result<Self::SerializeTupleVariant, Error> {
		Err(Error::new(
			"Cannot encode enums with data attached as keys, TT can only encode basic types as key",
		))
	}

	#[inline]
	fn serialize_none(self) -> Result<(), Error> {
		Err(Error::Empty())
	}

	// A present optional is represented as just the contained value. Note that
	// this is a lossy representation. For example the values `Some(())` and
	// `None` both serialize as just `null`. Unfortunately this is typically
	// what people expect when working with JSON. Other formats are encouraged
	// to behave more intelligently if possible.
	#[inline]
	fn serialize_some<T>(self, value: &T) -> Result<(), Error>
	where
		T: ?Sized + Serialize,
	{
		value.serialize(self)
	}

	fn is_human_readable(&self) -> bool {
		false
	}
}

pub struct KeyTyper {}

impl ser::Serializer for KeyTyper {
	type Ok = u8;
	type Error = Error;

	type SerializeSeq = Impossible<u8, Error>;
	type SerializeTuple = Impossible<u8, Error>;
	type SerializeTupleStruct = Impossible<u8, Error>;
	type SerializeTupleVariant = Impossible<u8, Error>;
	type SerializeMap = Impossible<u8, Error>;
	type SerializeStruct = Impossible<u8, Error>;
	type SerializeStructVariant = Impossible<u8, Error>;

	#[inline]
	fn serialize_str(self, _value: &str) -> Result<u8, Error> {
		Ok(serde::STRING.as_u8())
	}

	#[inline]
	fn serialize_char(self, _value: char) -> Result<u8, Error> {
		Ok(serde::STRING.as_u8())
	}

	#[inline]
	fn serialize_bytes(self, _value: &[u8]) -> Result<u8, Error> {
		Ok(serde::BYTES.as_u8())
	}
	#[inline]
	fn serialize_i8(self, _value: i8) -> Result<u8, Error> {
		Ok(serde::I8.as_u8())
	}
	#[inline]
	fn serialize_i16(self, _value: i16) -> Result<u8, Error> {
		Ok(serde::I16.as_u8())
	}
	#[inline]
	fn serialize_i32(self, _value: i32) -> Result<u8, Error> {
		Ok(serde::I32.as_u8())
	}
	#[inline]
	fn serialize_i64(self, _value: i64) -> Result<u8, Error> {
		Ok(serde::I64.as_u8())
	}
	#[inline]
	fn serialize_u8(self, _value: u8) -> Result<u8, Error> {
		Ok(serde::U8.as_u8())
	}
	#[inline]
	fn serialize_u16(self, _value: u16) -> Result<u8, Error> {
		Ok(serde::U16.as_u8())
	}
	#[inline]
	fn serialize_u32(self, _value: u32) -> Result<u8, Error> {
		Ok(serde::U32.as_u8())
	}
	#[inline]
	fn serialize_u64(self, _value: u64) -> Result<u8, Error> {
		Ok(serde::U64.as_u8())
	}
	#[inline]
	fn serialize_f32(self, _value: f32) -> Result<u8, Error> {
		Ok(serde::F32.as_u8())
	}
	#[inline]
	fn serialize_f64(self, _value: f64) -> Result<u8, Error> {
		Ok(serde::F64.as_u8())
	}
	#[inline]
	fn serialize_bool(self, _value: bool) -> Result<u8, Error> {
		Ok(serde::BOOL.as_u8())
	}

	// TT usually doesnt encode enums into keys, but simple enums can be described
	// using just a string so we well use that.
	fn serialize_unit_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		variant: &'static str,
	) -> Result<u8, Error> {
		self.serialize_str(variant)
	}

	fn serialize_newtype_struct<T>(self, _name: &'static str, _value: &T) -> Result<u8, Error>
	where
		T: ?Sized + Serialize,
	{
		Err(Error::ImpossibleKey(serde::MAP))
	}

	// We serialize this is a key-value
	fn serialize_newtype_variant<T>(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		_value: &T,
	) -> Result<u8, Error>
	where
		T: ?Sized + Serialize,
	{
		Err(Error::new(
			"Cannot encode enums with data attached as keys, TT can only encode basic types as key",
		))
	}

	// In Serde, unit means an anonymous value containing no data. Map this to
	// JSON as `null`.
	#[inline]
	fn serialize_unit(self) -> Result<u8, Error> {
		Err(Error::Empty())
	}

	// Unit struct means a named value containing no data. Again, since there is
	// no data, map this to JSON as `null`. There is no need to serialize the
	// name in most formats.
	fn serialize_unit_struct(self, _name: &'static str) -> Result<u8, Error> {
		self.serialize_unit()
	}

	fn serialize_seq(self, _len: Option<usize>) -> Result<Self::SerializeSeq, Error> {
		Err(Error::ImpossibleKey(serde::ARR))
	}

	fn serialize_tuple(self, len: usize) -> Result<Self::SerializeTuple, Error> {
		self.serialize_seq(Some(len))
	}

	// Tuple structs look just like sequences in JSON.
	fn serialize_tuple_struct(
		self,
		_name: &'static str,
		_len: usize,
	) -> Result<Self::SerializeTupleStruct, Error> {
		Err(Error::ImpossibleKey(serde::ARR))
	}

	fn serialize_struct(
		self,
		_name: &'static str,
		_len: usize,
	) -> Result<Self::SerializeStruct, Error> {
		Err(Error::ImpossibleKey(serde::MAP))
	}

	fn serialize_struct_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		_len: usize,
	) -> Result<Self::SerializeStructVariant, Error> {
		Err(Error::new(
			"Cannot encode enums with data attached as keys, TT can only encode basic types as key",
		))
	}

	// Maps are represented in JSON as `{ K: V, K: V, ... }`.
	fn serialize_map(self, _len: Option<usize>) -> Result<Self::SerializeMap, Error> {
		Err(Error::ImpossibleKey(serde::MAP))
	}

	// Tuple variants are represented in JSON as `{ NAME: [DATA...] }`. Again
	// this method is only responsible for the externally tagged representation.
	fn serialize_tuple_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		_len: usize,
	) -> Result<Self::SerializeTupleVariant, Error> {
		Err(Error::new(
			"Cannot encode enums with data attached as keys, TT can only encode basic types as key",
		))
	}

	#[inline]
	fn serialize_none(self) -> Result<u8, Error> {
		Err(Error::Empty())
	}

	// A present optional is represented as just the contained value. Note that
	// this is a lossy representation. For example the values `Some(())` and
	// `None` both serialize as just `null`. Unfortunately this is typically
	// what people expect when working with JSON. Other formats are encouraged
	// to behave more intelligently if possible.
	#[inline]
	fn serialize_some<T>(self, value: &T) -> Result<u8, Error>
	where
		T: ?Sized + Serialize,
	{
		value.serialize(self)
	}

	fn is_human_readable(&self) -> bool {
		false
	}
}
pub struct KeySizer {}

impl ser::Serializer for KeySizer {
	type Ok = usize;
	type Error = Error;

	type SerializeSeq = Impossible<usize, Error>;
	type SerializeTuple = Impossible<usize, Error>;
	type SerializeTupleStruct = Impossible<usize, Error>;
	type SerializeTupleVariant = Impossible<usize, Error>;
	type SerializeMap = Impossible<usize, Error>;
	type SerializeStruct = Impossible<usize, Error>;
	type SerializeStructVariant = Impossible<usize, Error>;

	#[inline]
	fn serialize_str(self, value: &str) -> Result<usize, Error> {
		Ok(value.len())
	}

	#[inline]
	fn serialize_char(self, value: char) -> Result<usize, Error> {
		// A char encoded as UTF-8 takes 4 bytes at most.
		let mut buf = [0; 4];
		self.serialize_str(value.encode_utf8(&mut buf))
	}

	#[inline]
	fn serialize_bytes(self, value: &[u8]) -> Result<usize, Error> {
		Ok(value.len())
	}
	#[inline]
	fn serialize_i8(self, _value: i8) -> Result<usize, Error> {
		Ok(1)
	}
	#[inline]
	fn serialize_i16(self, _value: i16) -> Result<usize, Error> {
		Ok(2)
	}
	#[inline]
	fn serialize_i32(self, _value: i32) -> Result<usize, Error> {
		Ok(4)
	}
	#[inline]
	fn serialize_i64(self, _value: i64) -> Result<usize, Error> {
		Ok(8)
	}
	#[inline]
	fn serialize_u8(self, _value: u8) -> Result<usize, Error> {
		Ok(1)
	}
	#[inline]
	fn serialize_u16(self, _value: u16) -> Result<usize, Error> {
		Ok(2)
	}
	#[inline]
	fn serialize_u32(self, _value: u32) -> Result<usize, Error> {
		Ok(4)
	}
	#[inline]
	fn serialize_u64(self, _value: u64) -> Result<usize, Error> {
		Ok(8)
	}
	#[inline]
	fn serialize_f32(self, _value: f32) -> Result<usize, Error> {
		Ok(4)
	}
	#[inline]
	fn serialize_f64(self, _value: f64) -> Result<usize, Error> {
		Ok(8)
	}
	#[inline]
	fn serialize_bool(self, _value: bool) -> Result<usize, Error> {
		Ok(1)
	}

	// TT usually doesnt encode enums into keys, but simple enums can be described
	// using just a string so we well use that.
	fn serialize_unit_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		variant: &'static str,
	) -> Result<usize, Error> {
		self.serialize_str(variant)
	}

	fn serialize_newtype_struct<T>(self, _name: &'static str, _value: &T) -> Result<usize, Error>
	where
		T: ?Sized + Serialize,
	{
		Err(Error::ImpossibleKey(serde::MAP))
	}

	// We serialize this is a key-value
	fn serialize_newtype_variant<T>(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		_value: &T,
	) -> Result<usize, Error>
	where
		T: ?Sized + Serialize,
	{
		Err(Error::new(
			"Cannot encode enums with data attached as keys, TT can only encode basic types as key",
		))
	}

	// In Serde, unit means an anonymous value containing no data. Map this to
	// JSON as `null`.
	#[inline]
	fn serialize_unit(self) -> Result<usize, Error> {
		Err(Error::Empty())
	}

	// Unit struct means a named value containing no data. Again, since there is
	// no data, map this to JSON as `null`. There is no need to serialize the
	// name in most formats.
	fn serialize_unit_struct(self, _name: &'static str) -> Result<usize, Error> {
		self.serialize_unit()
	}

	fn serialize_seq(self, _len: Option<usize>) -> Result<Self::SerializeSeq, Error> {
		Err(Error::ImpossibleKey(serde::ARR))
	}

	fn serialize_tuple(self, len: usize) -> Result<Self::SerializeTuple, Error> {
		self.serialize_seq(Some(len))
	}

	// Tuple structs look just like sequences in JSON.
	fn serialize_tuple_struct(
		self,
		_name: &'static str,
		_len: usize,
	) -> Result<Self::SerializeTupleStruct, Error> {
		Err(Error::ImpossibleKey(serde::ARR))
	}

	fn serialize_struct(
		self,
		_name: &'static str,
		_len: usize,
	) -> Result<Self::SerializeStruct, Error> {
		Err(Error::ImpossibleKey(serde::MAP))
	}

	fn serialize_struct_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		_len: usize,
	) -> Result<Self::SerializeStructVariant, Error> {
		Err(Error::new(
			"Cannot encode enums with data attached as keys, TT can only encode basic types as key",
		))
	}

	// Maps are represented in JSON as `{ K: V, K: V, ... }`.
	fn serialize_map(self, _len: Option<usize>) -> Result<Self::SerializeMap, Error> {
		Err(Error::ImpossibleKey(serde::MAP))
	}

	// Tuple variants are represented in JSON as `{ NAME: [DATA...] }`. Again
	// this method is only responsible for the externally tagged representation.
	fn serialize_tuple_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		_variant: &'static str,
		_len: usize,
	) -> Result<Self::SerializeTupleVariant, Error> {
		Err(Error::new(
			"Cannot encode enums with data attached as keys, TT can only encode basic types as key",
		))
	}

	#[inline]
	fn serialize_none(self) -> Result<usize, Error> {
		Err(Error::Empty())
	}

	// A present optional is represented as just the contained value. Note that
	// this is a lossy representation. For example the values `Some(())` and
	// `None` both serialize as just `null`. Unfortunately this is typically
	// what people expect when working with JSON. Other formats are encouraged
	// to behave more intelligently if possible.
	#[inline]
	fn serialize_some<T>(self, value: &T) -> Result<usize, Error>
	where
		T: ?Sized + Serialize,
	{
		value.serialize(self)
	}

	fn is_human_readable(&self) -> bool {
		false
	}
}
