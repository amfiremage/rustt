use crate::ttv3::size_var;
use serde::ser::{self, Serialize};
use std::fmt::{self, Display, Formatter};

pub struct ValueSizer {
	pub size: usize,
}

impl<'a> ser::Serializer for &'a mut ValueSizer {
	type Ok = ();
	type Error = EmptyError;

	type SerializeSeq = Compound<'a>;
	type SerializeTuple = Compound<'a>;
	type SerializeTupleStruct = Compound<'a>;
	type SerializeTupleVariant = Compound<'a>;
	type SerializeMap = Compound<'a>;
	type SerializeStruct = Compound<'a>;
	type SerializeStructVariant = Compound<'a>;

	#[inline]
	fn serialize_str(self, value: &str) -> Result<(), EmptyError> {
		//+1 for 0 children_length, +1 for the type
		let len = value.len();
		Ok(self.size += size_var(len) + len + 1 + 1)
	}

	#[inline]
	fn serialize_char(self, _value: char) -> Result<(), EmptyError> {
		//+1 for required varint, +4 for binary length, +1 for 0 children_length, +1 for the type
		Ok(self.size += 1 + 4 + 1 + 1)
	}

	#[inline]
	fn serialize_bytes(self, value: &[u8]) -> Result<(), EmptyError> {
		//+1 for 0 children_length, +1 for the type
		let len = value.len();
		Ok(self.size += size_var(len) + len + 1 + 1)
	}
	#[inline]
	fn serialize_i8(self, _value: i8) -> Result<(), EmptyError> {
		//+1 for required varint, +1 for binary length, +1 for 0 children_length, +1 for the type
		Ok(self.size += 1 + 1 + 1 + 1)
	}
	#[inline]
	fn serialize_i16(self, _value: i16) -> Result<(), EmptyError> {
		//+1 for required varint, +2 for binary length, +1 for 0 children_length, +1 for the type
		Ok(self.size += 1 + 2 + 1 + 1)
	}
	#[inline]
	fn serialize_i32(self, _value: i32) -> Result<(), EmptyError> {
		//+1 for required varint, +4 for binary length, +1 for 0 children_length, +1 for the type
		Ok(self.size += 1 + 4 + 1 + 1)
	}
	#[inline]
	fn serialize_i64(self, _value: i64) -> Result<(), EmptyError> {
		//+1 for required varint, +8 for binary length, +1 for 0 children_length, +1 for the type
		Ok(self.size += 1 + 8 + 1 + 1)
	}
	#[inline]
	fn serialize_u8(self, _value: u8) -> Result<(), EmptyError> {
		//+1 for required varint, +1 for binary length, +1 for 0 children_length, +1 for the type
		Ok(self.size += 1 + 1 + 1 + 1)
	}
	#[inline]
	fn serialize_u16(self, _value: u16) -> Result<(), EmptyError> {
		//+1 for required varint, +2 for binary length, +1 for 0 children_length, +1 for the type
		Ok(self.size += 1 + 2 + 1 + 1)
	}
	#[inline]
	fn serialize_u32(self, _value: u32) -> Result<(), EmptyError> {
		//+1 for required varint, +4 for binary length, +1 for 0 children_length, +1 for the type
		Ok(self.size += 1 + 4 + 1 + 1)
	}
	#[inline]
	fn serialize_u64(self, _value: u64) -> Result<(), EmptyError> {
		//+1 for required varint, +8 for binary length, +1 for 0 children_length, +1 for the type
		Ok(self.size += 1 + 8 + 1 + 1)
	}
	#[inline]
	fn serialize_f32(self, _value: f32) -> Result<(), EmptyError> {
		//+1 for required varint, +4 for binary length, +1 for 0 children_length, +1 for the type
		Ok(self.size += 1 + 4 + 1 + 1)
	}
	#[inline]
	fn serialize_f64(self, _value: f64) -> Result<(), EmptyError> {
		//+1 for required varint, +8 for binary length, +1 for 0 children_length, +1 for the type
		Ok(self.size += 1 + 8 + 1 + 1)
	}
	#[inline]
	fn serialize_bool(self, _value: bool) -> Result<(), EmptyError> {
		//+1 for required varint, +1 for binary length, +1 for 0 children_length, +1 for the type
		Ok(self.size += 1 + 1 + 1 + 1)
	}

	#[inline]
	fn serialize_newtype_struct<T>(self, _name: &'static str, value: &T) -> Result<(), EmptyError>
	where
		T: ?Sized + Serialize,
	{
		value.serialize(self)
	}

	// Maps are represented in JSON as `{ K: V, K: V, ... }`.
	#[inline]
	fn serialize_map(self, len: Option<usize>) -> Result<Self::SerializeMap, EmptyError> {
		match len {
			Some(l) => {
				//+1 for required varint, +0 for binary length, +1 for the type
				self.size += 1 + 0 + size_var(l) + 1;
				Ok(Compound { ser: self })
			}
			None => Ok(Compound { ser: self }),
		}
	}

	#[inline]
	fn serialize_struct(
		self,
		_name: &'static str,
		len: usize,
	) -> Result<Self::SerializeStruct, EmptyError> {
		//+1 for required varint, +0 for binary length, +1 for the type
		self.size += 1 + 0 + size_var(len) + 1;
		Ok(Compound { ser: self })
	}

	#[inline]
	fn serialize_struct_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		variant: &'static str,
		len: usize,
	) -> Result<Self::SerializeStructVariant, EmptyError> {
		//the 1 length map value
		//+1 for required varint, +0 for binary length,+1 for the children_length, +1 for the type
		self.size += 1 + 0 + 1 + 1;
		//the `len` length map value
		//+1 for required varint, +0 for binary length, +1 for the type
		self.size += 1 + 0 + size_var(len) + 1;
		//the key
		//+1 for the type
		let len = variant.len();
		self.size += size_var(len) + len + 1;

		Ok(Compound { ser: self })
	}

	// TT is platform independant and self-describing. If the order of varints is diferent on
	// the reaceiver compred to the sender it wont work thus the name was chosen
	#[inline]
	fn serialize_unit_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		variant: &'static str,
	) -> Result<(), EmptyError> {
		//+1 for required varint, +0 for binary length,+1 for the children_length, +1 for the type
		self.size += 1 + 0 + 1 + 1;
		variant.serialize(self)
	}
	// We serialize this is a key-value
	#[inline]
	fn serialize_newtype_variant<T>(
		self,
		_name: &'static str,
		_variant_index: u32,
		variant: &'static str,
		value: &T,
	) -> Result<(), EmptyError>
	where
		T: ?Sized + Serialize,
	{
		//+1 for required varint, +0 for binary length,+1 for the children_length, +1 for the type
		self.size += 1 + 0 + 1 + 1;

		//the key
		//+1 for the type
		let len = variant.len();
		self.size += size_var(len) + len + 1;

		value.serialize(self)
	}
	// Tuple variants are represented in JSON as `{ NAME: [DATA...] }`. Again
	// this method is only responsible for the externally tagged representation.
	#[inline]
	fn serialize_tuple_variant(
		self,
		_name: &'static str,
		_variant_index: u32,
		variant: &'static str,
		len: usize,
	) -> Result<Self::SerializeTupleVariant, EmptyError> {
		//the 1 length map value
		//+1 for required varint, +0 for binary length,+1 for the children_length, +1 for the type
		self.size += 1 + 0 + 1 + 1;
		//the `len` length ARR value
		//+1 for required varint, +0 for binary length, +1 for the type
		self.size += 1 + 0 + size_var(len) + 1;

		//the key for the second ARR
		//+1 for the type
		let len = variant.len();
		self.size += size_var(len) + len + 1;

		Ok(Compound { ser: self })
	}
	#[inline]
	fn serialize_tuple(self, len: usize) -> Result<Self::SerializeTuple, EmptyError> {
		//+1 for required varint, +0 for binary length, +1 for the type
		self.size += 1 + 0 + size_var(len) + 1;
		Ok(Compound { ser: self })
	}

	// Tuple structs look just like sequences in JSON.
	#[inline]
	fn serialize_tuple_struct(
		self,
		_name: &'static str,
		len: usize,
	) -> Result<Self::SerializeTupleStruct, EmptyError> {
		//+1 for required varint, +0 for binary length, +1 for the type
		self.size += 1 + 0 + size_var(len) + 1;
		Ok(Compound { ser: self })
	}
	#[inline]
	fn serialize_seq(self, len: Option<usize>) -> Result<Self::SerializeSeq, EmptyError> {
		match len {
			Some(l) => {
				//+1 for required varint, +0 for binary length, +1 for the type
				self.size += 1 + 0 + size_var(l) + 1;
				Ok(Compound { ser: self })
			}
			None => return Err(EmptyError {}),
		}
	}

	// In Serde, unit means an anonymous value containing no data. Map this to
	// JSON as `null`.
	#[inline]
	fn serialize_unit(self) -> Result<(), EmptyError> {
		Ok(())
	}

	// Unit struct means a named value containing no data. Again, since there is
	// no data, map this to JSON as `null`. There is no need to serialize the
	// name in most formats.
	#[inline]
	fn serialize_unit_struct(self, _name: &'static str) -> Result<(), EmptyError> {
		self.serialize_unit()
	}
	#[inline]
	fn serialize_none(self) -> std::result::Result<(), EmptyError> {
		self.serialize_unit()
	}
	// A present optional is represented as just the contained value. Note that
	// this is a lossy representation. For example the values `Some(())` and
	// `None` both serialize as just `null`. Unfortunately this is typically
	// what people expect when working with JSON. Other formats are encouraged
	// to behave more intelligently if possible.
	#[inline]
	fn serialize_some<T>(self, value: &T) -> Result<(), EmptyError>
	where
		T: ?Sized + Serialize,
	{
		value.serialize(self)
	}
}

pub struct Compound<'a> {
	ser: &'a mut ValueSizer,
}

impl<'a> ser::SerializeTupleVariant for Compound<'a> {
	type Ok = ();
	type Error = EmptyError;

	#[inline]
	fn serialize_field<T: ?Sized + Serialize>(&mut self, value: &T) -> Result<(), EmptyError> {
		//+1 for the key varint, +1 for the empty key type
		self.ser.size += 1 + 1;
		value.serialize(&mut *self.ser)
	}
	#[inline]
	fn end(self) -> Result<(), EmptyError> {
		Ok(())
	}
}
impl<'a> ser::SerializeSeq for Compound<'a> {
	type Ok = ();
	type Error = EmptyError;

	#[inline]
	fn serialize_element<T: ?Sized + Serialize>(&mut self, value: &T) -> Result<(), EmptyError> {
		//+1 for the key varint, +1 for the empty key type
		self.ser.size += 1 + 1;
		value.serialize(&mut *self.ser)
	}
	#[inline]
	fn end(self) -> Result<(), EmptyError> {
		Ok(())
	}
}
impl<'a> ser::SerializeTuple for Compound<'a> {
	type Ok = ();
	type Error = EmptyError;

	#[inline]
	fn serialize_element<T: ?Sized + Serialize>(&mut self, value: &T) -> Result<(), EmptyError> {
		//+1 for the key varint, +1 for the empty key type
		self.ser.size += 1 + 1;
		value.serialize(&mut *self.ser)
	}
	#[inline]
	fn end(self) -> Result<(), EmptyError> {
		Ok(())
	}
}
impl<'a> ser::SerializeTupleStruct for Compound<'a> {
	type Ok = ();
	type Error = EmptyError;

	#[inline]
	fn serialize_field<T: ?Sized>(&mut self, value: &T) -> Result<(), EmptyError>
	where
		T: Serialize,
	{
		//+1 for the key varint, +1 for the empty key type
		self.ser.size += 1 + 1;
		value.serialize(&mut *self.ser)
	}
	#[inline]
	fn end(self) -> Result<(), EmptyError> {
		Ok(())
	}
}
impl<'a> ser::SerializeMap for Compound<'a> {
	type Ok = ();
	type Error = EmptyError;

	#[inline]
	fn serialize_key<T>(&mut self, key: &T) -> Result<(), EmptyError>
	where
		T: ?Sized + Serialize,
	{
		//the serialize always ads one for the children_length but keys dont have that.
		let _ = key.serialize(&mut *self.ser);
		self.ser.size -= 1;
		Ok(())
	}

	#[inline]
	fn serialize_value<T>(&mut self, value: &T) -> Result<(), EmptyError>
	where
		T: ?Sized + Serialize,
	{
		value.serialize(&mut *self.ser)
	}

	#[inline]
	fn end(self) -> Result<(), EmptyError> {
		Ok(())
	}
}
impl<'a> ser::SerializeStruct for Compound<'a> {
	type Ok = ();
	type Error = EmptyError;

	#[inline]
	fn serialize_field<T: ?Sized>(&mut self, key: &'static str, value: &T) -> Result<(), EmptyError>
	where
		T: Serialize,
	{
		// +1 for the type
		let len = key.len();
		self.ser.size += size_var(len) + len + 1;
		let _ = value.serialize(&mut *self.ser);
		Ok(())
	}

	#[inline]
	fn end(self) -> Result<(), EmptyError> {
		Ok(())
	}
}
impl<'a> ser::SerializeStructVariant for Compound<'a> {
	type Ok = ();
	type Error = EmptyError;

	#[inline]
	fn serialize_field<T: ?Sized>(&mut self, key: &'static str, value: &T) -> Result<(), EmptyError>
	where
		T: Serialize,
	{
		// +1 for the type
		let len = key.len();
		self.ser.size += size_var(len) + len + 1;
		let _ = value.serialize(&mut *self.ser);
		Ok(())
	}

	#[inline]
	fn end(self) -> Result<(), EmptyError> {
		Ok(())
	}
}

#[derive(Clone, Debug, PartialEq)]
pub struct EmptyError {}

impl std::error::Error for EmptyError {}
impl ser::Error for EmptyError {
	fn custom<T: Display>(_msg: T) -> Self {
		EmptyError {}
	}
}
impl Display for EmptyError {
	fn fmt(&self, formatter: &mut Formatter) -> fmt::Result {
		formatter.write_str("")
	}
}

impl std::convert::From<std::str::Utf8Error> for EmptyError {
	fn from(_err: std::str::Utf8Error) -> EmptyError {
		EmptyError {}
	}
}

impl std::convert::From<std::io::Error> for EmptyError {
	fn from(_err: std::io::Error) -> EmptyError {
		EmptyError {}
	}
}
