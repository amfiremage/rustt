use crate::ttv3::serde::{TtF32, TtF64};
use crate::ttv3::untyped::Key;
use crate::ttv3::untyped::Value;
use serde::de::{Deserialize, MapAccess, SeqAccess, Visitor};

pub struct KeyVisitor;

impl<'de> Visitor<'de> for KeyVisitor {
	type Value = Key;

	fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
		formatter.write_str("any valid TT key")
	}

	#[inline]
	fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
	where
		E: serde::de::Error,
	{
		self.visit_string(String::from(value))
	}

	#[inline]
	fn visit_string<E>(self, value: String) -> Result<Self::Value, E> {
		Ok(Self::Value::String(value))
	}

	#[inline]
	fn visit_byte_buf<E>(self, value: Vec<u8>) -> Result<Self::Value, E> {
		Ok(Self::Value::Bytes(value))
	}

	#[inline]
	fn visit_bytes<E>(self, value: &[u8]) -> Result<Self::Value, E> {
		Ok(Self::Value::Bytes(value.to_vec()))
	}

	#[inline]
	fn visit_bool<E>(self, value: bool) -> Result<Self::Value, E> {
		Ok(Self::Value::Bool(value))
	}

	#[inline]
	fn visit_i8<E>(self, value: i8) -> Result<Self::Value, E> {
		Ok(Self::Value::I8(value))
	}
	#[inline]
	fn visit_i16<E>(self, value: i16) -> Result<Self::Value, E> {
		Ok(Self::Value::I16(value))
	}
	#[inline]
	fn visit_i32<E>(self, value: i32) -> Result<Self::Value, E> {
		Ok(Self::Value::I32(value))
	}

	#[inline]
	fn visit_i64<E>(self, value: i64) -> Result<Self::Value, E> {
		Ok(Self::Value::I64(value))
	}

	#[inline]
	fn visit_u8<E>(self, value: u8) -> Result<Self::Value, E> {
		Ok(Self::Value::U8(value))
	}
	#[inline]
	fn visit_u16<E>(self, value: u16) -> Result<Self::Value, E> {
		Ok(Self::Value::U16(value))
	}
	#[inline]
	fn visit_u32<E>(self, value: u32) -> Result<Self::Value, E> {
		Ok(Self::Value::U32(value))
	}
	#[inline]
	fn visit_u64<E>(self, value: u64) -> Result<Self::Value, E> {
		Ok(Self::Value::U64(value))
	}

	#[inline]
	fn visit_f64<E>(self, value: f64) -> Result<Self::Value, E> {
		Ok(Self::Value::F64(TtF64::from_f64(value)))
	}
	#[inline]
	fn visit_f32<E>(self, value: f32) -> Result<Self::Value, E> {
		Ok(Self::Value::F32(TtF32::from_f32(value)))
	}

	#[inline]
	fn visit_none<E>(self) -> Result<Self::Value, E>
	where
		E: serde::de::Error,
	{
		Err(serde::de::Error::custom("None is not a valid type"))
	}

	#[inline]
	fn visit_some<D>(self, deserializer: D) -> Result<Self::Value, D::Error>
	where
		D: serde::Deserializer<'de>,
	{
		Deserialize::deserialize(deserializer)
	}

	#[inline]
	fn visit_unit<E>(self) -> Result<Self::Value, E>
	where
		E: serde::de::Error,
	{
		Err(serde::de::Error::custom("Empty types are not valid"))
	}
}

pub struct ValueVisitor;

impl<'de> Visitor<'de> for ValueVisitor {
	type Value = Value;

	fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
		formatter.write_str("any valid TT value")
	}

	#[inline]
	fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
	where
		E: serde::de::Error,
	{
		self.visit_string(String::from(value))
	}

	#[inline]
	fn visit_string<E>(self, value: String) -> Result<Self::Value, E> {
		Ok(Self::Value::String(value))
	}

	#[inline]
	fn visit_byte_buf<E>(self, value: Vec<u8>) -> Result<Self::Value, E> {
		Ok(Self::Value::Bytes(value))
	}

	#[inline]
	fn visit_bytes<E>(self, value: &[u8]) -> Result<Self::Value, E> {
		Ok(Self::Value::Bytes(value.to_vec()))
	}

	#[inline]
	fn visit_bool<E>(self, value: bool) -> Result<Self::Value, E> {
		Ok(Self::Value::Bool(value))
	}

	#[inline]
	fn visit_i8<E>(self, value: i8) -> Result<Self::Value, E> {
		Ok(Self::Value::I8(value))
	}

	#[inline]
	fn visit_i16<E>(self, value: i16) -> Result<Self::Value, E> {
		Ok(Self::Value::I16(value))
	}

	#[inline]
	fn visit_i32<E>(self, value: i32) -> Result<Self::Value, E> {
		Ok(Self::Value::I32(value))
	}

	#[inline]
	fn visit_i64<E>(self, value: i64) -> Result<Self::Value, E> {
		Ok(Self::Value::I64(value))
	}

	#[inline]
	fn visit_u8<E>(self, value: u8) -> Result<Self::Value, E> {
		Ok(Self::Value::U8(value))
	}

	#[inline]
	fn visit_u16<E>(self, value: u16) -> Result<Self::Value, E> {
		Ok(Self::Value::U16(value))
	}

	#[inline]
	fn visit_u32<E>(self, value: u32) -> Result<Self::Value, E> {
		Ok(Self::Value::U32(value))
	}

	#[inline]
	fn visit_u64<E>(self, value: u64) -> Result<Self::Value, E> {
		Ok(Self::Value::U64(value))
	}

	#[inline]
	fn visit_f32<E>(self, value: f32) -> Result<Self::Value, E> {
		Ok(Self::Value::F32(value))
	}

	#[inline]
	fn visit_f64<E>(self, value: f64) -> Result<Self::Value, E> {
		Ok(Self::Value::F64(value))
	}

	#[inline]
	fn visit_some<D>(self, deserializer: D) -> Result<Value, D::Error>
	where
		D: serde::Deserializer<'de>,
	{
		Deserialize::deserialize(deserializer)
	}

	fn visit_seq<V>(self, mut visitor: V) -> Result<Self::Value, V::Error>
	where
		V: SeqAccess<'de>,
	{
		let mut vec = match visitor.size_hint() {
			Some(size) => Vec::with_capacity(size),
			None => Vec::new(),
		};

		while let Some(elem) = visitor.next_element()? {
			vec.push(elem);
		}

		Ok(Self::Value::Vec(vec))
	}

	fn visit_map<V>(self, mut visitor: V) -> Result<Self::Value, V::Error>
	where
		V: MapAccess<'de>,
	{
		let mut map: std::collections::HashMap<Key, Self::Value> = match visitor.size_hint() {
			Some(size) => std::collections::HashMap::with_capacity(size),
			None => std::collections::HashMap::new(),
		};

		while let Some((key, value)) = visitor.next_entry()? {
			map.insert(key, value);
		}
		Ok(Self::Value::Map(map))
	}
}
