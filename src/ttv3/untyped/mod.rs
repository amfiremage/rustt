use serde::de::{Deserialize, Deserializer};
use serde::ser::{Serialize, SerializeMap, SerializeSeq, Serializer};

mod de;
pub use de::{KeyVisitor, ValueVisitor};

#[derive(Hash, PartialEq, Eq, Debug)]
pub enum Key {
	String(String),
	Bytes(Vec<u8>),
	I8(i8),
	I16(i16),
	I32(i32),
	I64(i64),
	U8(u8),
	U16(u16),
	U32(u32),
	U64(u64),
	Bool(bool),
	F32(crate::ttv3::serde::TtF32),
	F64(crate::ttv3::serde::TtF64),
}

impl<'de> Deserialize<'de> for Key {
	#[inline]
	fn deserialize<D>(deserializer: D) -> Result<Key, D::Error>
	where
		D: Deserializer<'de>,
	{
		deserializer.deserialize_any(crate::ttv3::untyped::KeyVisitor)
	}
}

impl Serialize for Key {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: Serializer,
	{
		match self {
			Key::String(s) => serializer.serialize_str(s),
			Key::Bytes(b) => serializer.serialize_bytes(b),
			Key::I8(i) => serializer.serialize_i8(*i),
			Key::I16(i) => serializer.serialize_i16(*i),
			Key::I32(i) => serializer.serialize_i32(*i),
			Key::I64(i) => serializer.serialize_i64(*i),
			Key::U8(u) => serializer.serialize_u8(*u),
			Key::U16(u) => serializer.serialize_u16(*u),
			Key::U32(u) => serializer.serialize_u32(*u),
			Key::U64(u) => serializer.serialize_u64(*u),
			Key::Bool(b) => serializer.serialize_bool(*b),
			Key::F32(ttf32) => serializer.serialize_f32(ttf32.into()),
			Key::F64(ttf64) => serializer.serialize_f64(ttf64.into()),
		}
	}
}

#[derive(Debug)]
pub enum Value {
	String(String),
	Bytes(Vec<u8>),
	I8(i8),
	I16(i16),
	I32(i32),
	I64(i64),
	U8(u8),
	U16(u16),
	U32(u32),
	U64(u64),
	Bool(bool),
	F32(f32),
	F64(f64),
	Map(std::collections::HashMap<Key, Value>),
	Vec(Vec<Value>),
}

impl<'de> Deserialize<'de> for Value {
	#[inline]
	fn deserialize<D>(deserializer: D) -> Result<Value, D::Error>
	where
		D: Deserializer<'de>,
	{
		deserializer.deserialize_any(crate::ttv3::untyped::ValueVisitor)
	}
}

impl Serialize for Value {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: Serializer,
	{
		match self {
			Value::String(s) => serializer.serialize_str(s),
			Value::Bytes(b) => serializer.serialize_bytes(b),
			Value::I8(i) => serializer.serialize_i8(*i),
			Value::I16(i) => serializer.serialize_i16(*i),
			Value::I32(i) => serializer.serialize_i32(*i),
			Value::I64(i) => serializer.serialize_i64(*i),
			Value::U8(u) => serializer.serialize_u8(*u),
			Value::U16(u) => serializer.serialize_u16(*u),
			Value::U32(u) => serializer.serialize_u32(*u),
			Value::U64(u) => serializer.serialize_u64(*u),
			Value::Bool(b) => serializer.serialize_bool(*b),
			Value::F32(f) => serializer.serialize_f32(*f),
			Value::F64(f) => serializer.serialize_f64(*f),
			Value::Map(m) => {
				let mut map = serializer.serialize_map(Some(m.len()))?;
				for (k, v) in m {
					map.serialize_entry(k, v)?;
				}
				map.end()
			}
			Value::Vec(m) => {
				let mut seq = serializer.serialize_seq(Some(m.len()))?;
				for e in m {
					seq.serialize_element(e)?;
				}
				seq.end()
			}
		}
	}
}
