#[macro_use]
extern crate criterion;
extern crate rustt as tt;
//use cpuprofiler::PROFILER;

use criterion::black_box;
use criterion::Criterion;
use serde::{Deserialize, Serialize};
use tt::ttv3::Key;
use tt::ttv3::Value;

criterion_main!(
	worst_case_serde_json_static,
	worst_case_serde_static_rwv3,
	worst_case_serde_staticv3,
	worst_case_serdev3,
	u64_serdev3,
);

#[derive(Serialize, Deserialize)]
struct SubWorstCase {
	#[serde(rename = "more")]
	more: String,
}

#[derive(Serialize, Deserialize)]
struct WorstCase {
	#[serde(rename = "Da5ta")]
	data: String,
	#[serde(rename = "Data2")]
	subdata: SubWorstCase,
	#[serde(rename = "Data4")]
	sublist: Vec<String>,
	#[serde(rename = "1", with = "serde_bytes")]
	subbytes: Vec<u8>,
	#[serde(rename = "2")]
	float: f64,
	#[serde(rename = "3")]
	int: i64,
	#[serde(rename = "4")]
	bool: bool,
}
criterion_group! {
	name = worst_case_serde_json_static;
	config = Criterion::default().confidence_level(0.01).measurement_time(std::time::Duration::new(20, 0));
	targets =  bench_enc_dec_worst_case_serde_json_static ,
}

fn bench_enc_dec_worst_case_serde_json_static(c: &mut Criterion) {
	let w = WorstCase {
		data: String::from("n0thing"),
		subdata: SubWorstCase {
			more: String::from("d5ata89"),
		},
		sublist: vec![String::from("hey"), String::from("jude")],
		subbytes: vec![b'h', b'i'],
		float: 0.64,
		int: 99,
		bool: true,
	};

	c.bench_function("enc_dec_worst_case_serde_json_static", |b| {
		b.iter(|| {
			let data = serde_json::to_string(&w).unwrap();
			let m: WorstCase = serde_json::from_str(&data).unwrap();
			black_box(m);
		})
	});
	c.bench_function("enc_worst_case_serde_json_static", |b| {
		b.iter(|| {
			black_box(serde_json::to_string(&w).unwrap());
		})
	});
	c.bench_function("dec_worst_case_serde_json_static", |b| {
		let data = serde_json::to_string(&w).unwrap();
		b.iter(|| {
			let m: WorstCase = serde_json::from_str(&data).unwrap();
			black_box(m);
		})
	});
}

criterion_group! {
	name = worst_case_serde_static_rwv3;
	config = Criterion::default().confidence_level(0.01).measurement_time(std::time::Duration::new(20, 0));
	targets =  bench_enc_dec_worst_case_serde_static_rwv3 ,
}

fn bench_enc_dec_worst_case_serde_static_rwv3(c: &mut Criterion) {
	let w = WorstCase {
		data: String::from("n0thing"),
		subdata: SubWorstCase {
			more: String::from("d5ata89"),
		},
		sublist: vec![String::from("hey"), String::from("jude")],
		subbytes: vec![b'h', b'i'],
		float: 0.64,
		int: 99,
		bool: true,
	};

	c.bench_function("enc_dec_worst_case_serde_static_rwv3", |b| {
		b.iter(|| {
			let data = tt::to_vec(&w).unwrap();
			let m: WorstCase = tt::from_reader(&data[..]).unwrap();
			black_box(m);
		})
	});
	c.bench_function("enc_worst_case_serde_static_rwv3", |b| {
		b.iter(|| {
			black_box(tt::to_vec(&w)).unwrap();
		});
	});
	c.bench_function("dec_worst_case_serde_static_rwv3", |b| {
		let data = tt::to_vec(&w).unwrap();
		b.iter(|| {
			let m: WorstCase = tt::from_reader(&data[..]).unwrap();
			black_box(m);
		})
	});
}

criterion_group! {
	name = worst_case_serde_staticv3;
	config = Criterion::default().confidence_level(0.01).measurement_time(std::time::Duration::new(20, 0));
	targets =  bench_enc_dec_worst_case_serde_staticv3 ,
}

fn bench_enc_dec_worst_case_serde_staticv3(c: &mut Criterion) {
	let w = WorstCase {
		data: String::from("n0thing"),
		subdata: SubWorstCase {
			more: String::from("d5ata89"),
		},
		sublist: vec![String::from("hey"), String::from("jude")],
		subbytes: vec![b'h', b'i'],
		float: 0.64,
		int: 99,
		bool: true,
	};
	c.bench_function("enc_dec_worst_case_serde_staticv3", |b| {
		b.iter(|| {
			let data = tt::to_vec(&w).unwrap();
			let m: WorstCase = tt::from_bytes(&data).unwrap();
			black_box(m);
		})
	});
	c.bench_function("enc_worst_case_serde_staticv3", |b| {
		b.iter(|| {
			black_box(tt::to_vec(&w).unwrap());
		});
	});
	c.bench_function("dec_worst_case_serde_staticv3", |b| {
		let data = tt::to_vec(&w).unwrap();
		b.iter(|| {
			let m: WorstCase = tt::from_bytes(&data).unwrap();
			black_box(m);
		})
	});
}

criterion_group! {
	name = worst_case_serdev3;
	config = Criterion::default().confidence_level(0.01).measurement_time(std::time::Duration::new(20, 0));
	targets =  bench_enc_dec_worst_case_serdev3,
}

fn bench_enc_dec_worst_case_serdev3(c: &mut Criterion) {
	let mut map: std::collections::HashMap<&str, Value> = std::collections::HashMap::new();
	map.insert(&"Da5ta", Value::String("n0thing".to_string()));
	let mut submap: std::collections::HashMap<Key, Value> = std::collections::HashMap::new();
	submap.insert(
		Key::String("more".to_string()),
		Value::String("d5ata89".to_string()),
	);
	map.insert(&"Data2", Value::Map(submap));
	let vec: Vec<Value> = vec![
		Value::String("hey".to_string()),
		Value::String("jude".to_string()),
	];
	map.insert(&"Data4", Value::Vec(vec));
	let vec2 = vec![1, 2];
	map.insert(&"1", Value::Bytes(vec2));
	map.insert(&"2", Value::F64(0.64 as f64));
	map.insert(&"3", Value::I64(99 as i64));
	map.insert(&"4", Value::Bool(true));

	c.bench_function("enc_dec_worst_case_serdev3", |b| {
		b.iter(|| {
			let data = tt::to_vec(&map).unwrap();
			let m: std::collections::HashMap<&str, Value> = tt::from_bytes(&data).unwrap();
			black_box(m);
		})
	});
	c.bench_function("enc_worst_case_serdev3", |b| {
		b.iter(|| {
			black_box(tt::to_vec(&map).unwrap());
		});
	});
	c.bench_function("dec_worst_case_serdev3", |b| {
		let data = tt::to_vec(&map).unwrap();
		b.iter(|| {
			let m: std::collections::HashMap<&str, Value> = tt::from_bytes(&data).unwrap();
			black_box(m);
		})
	});
}

criterion_group! {
	name = u64_serdev3;
	config = Criterion::default().confidence_level(0.01).measurement_time(std::time::Duration::new(60, 0));
	targets =  bench_enc_dec_u64_serdev3,
}

fn bench_enc_dec_u64_serdev3(c: &mut Criterion) {
	let i: u64 = 46372;
	c.bench_function("enc_dec_u64_serdev3", |b| {
		b.iter(|| {
			let data = tt::to_vec(&i).unwrap();
			let m: u64 = tt::from_bytes(&data).unwrap();
			black_box(m);
		})
	});
	c.bench_function("enc_u64_serdev3", |b| {
		b.iter(|| {
			black_box(tt::to_vec(&i).unwrap());
		});
	});
	c.bench_function("dec_u64_serdev3", |b| {
		let data = tt::to_vec(&i).unwrap();
		b.iter(|| {
			let m: u64 = tt::from_bytes(&data).unwrap();
			black_box(m);
		})
	});
}
